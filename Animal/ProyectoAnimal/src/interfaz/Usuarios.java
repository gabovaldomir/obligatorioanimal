package interfaz;

import dominio.*;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * @author gabomir
 * @author natagarc
 */
public class Usuarios extends javax.swing.JPanel {
    private Sistema sistema;
    private boolean agregarUsuarioSeleccionado;
    
    public Usuarios(Sistema sis) {
        sistema = sis;
        initComponents();
        agregarUsuarioSeleccionado = true;
        usuarioPanDeposito.setVisible(false);
        usuarioPanActividades.setVisible(false);
        this.setSize(950, 625);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panUsuarios = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        UsuarioBtnAgregar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        UsuarioLblNombre = new javax.swing.JLabel();
        UsuarioLblApellido = new javax.swing.JLabel();
        UsuarioLblTel = new javax.swing.JLabel();
        UsuarioLblMail = new javax.swing.JLabel();
        UsuarioLblCiudad = new javax.swing.JLabel();
        UsuarioLblPais = new javax.swing.JLabel();
        UsuarioCBPadrino = new javax.swing.JCheckBox();
        UsuarioTxtPais = new javax.swing.JTextField();
        UsuarioTxtCiudad = new javax.swing.JTextField();
        UsuarioTxtMail = new javax.swing.JTextField();
        UsuarioTxtTel = new javax.swing.JFormattedTextField();
        UsuarioTxtApellido = new javax.swing.JTextField();
        UsuarioTxtNombre = new javax.swing.JTextField();
        UsuarioComboUsuarios = new javax.swing.JComboBox<>();
        UsuarioLblUsuarios = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        usuarioPanActividades = new javax.swing.JPanel();
        UsuarioScrollFechas = new javax.swing.JScrollPane();
        UsuarioLstFechas = new javax.swing.JList<>();
        UsuarioScrollActividades = new javax.swing.JScrollPane();
        UsuarioLstActividades = new javax.swing.JList<>();
        UsuarioLblActividad = new javax.swing.JLabel();
        UsuarioLblFecha = new javax.swing.JLabel();
        UsuarioLblActividades = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        usuarioPanDeposito = new javax.swing.JPanel();
        UsuarioLblValor = new javax.swing.JLabel();
        UsuarioLblMoneda = new javax.swing.JLabel();
        UsuarioLblPeriodo = new javax.swing.JLabel();
        UsuarioLblMedio = new javax.swing.JLabel();
        UsuarioTxtValor = new javax.swing.JFormattedTextField();
        UsuarioComboMoneda = new javax.swing.JComboBox<>();
        UsuarioComboPeriodo = new javax.swing.JComboBox<>();
        UsuarioComboMedio = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        UsuarioLstAnimales = new javax.swing.JList<>();
        UsuarioLblValor1 = new javax.swing.JLabel();
        UsuarioBtnEliminar = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(852, 633));

        panUsuarios.setMaximumSize(new java.awt.Dimension(853, 633));
        panUsuarios.setPreferredSize(new java.awt.Dimension(833, 572));

        UsuarioBtnAgregar.setText("Agregar Usuario");
        UsuarioBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioBtnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(UsuarioBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(785, 785, 785))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(UsuarioBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1.setRequestFocusEnabled(false);

        UsuarioLblNombre.setText("Nombre:");

        UsuarioLblApellido.setText("Apellido:");

        UsuarioLblTel.setText("Teléfono:");

        UsuarioLblMail.setText("Mail:");

        UsuarioLblCiudad.setText("Ciudad:");

        UsuarioLblPais.setText("País:");

        UsuarioCBPadrino.setText("Es padrino");
        UsuarioCBPadrino.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioCBPadrino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioCBPadrinoActionPerformed(evt);
            }
        });

        UsuarioTxtPais.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioTxtCiudad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioTxtMail.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioTxtMail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioTxtMailActionPerformed(evt);
            }
        });

        UsuarioTxtTel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioTxtTel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0"))));
        UsuarioTxtTel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioTxtTelActionPerformed(evt);
            }
        });

        UsuarioTxtApellido.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioTxtNombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(UsuarioCBPadrino)
                        .addGap(0, 54, Short.MAX_VALUE))
                    .addComponent(UsuarioLblCiudad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UsuarioLblMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UsuarioLblTel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UsuarioLblApellido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UsuarioLblNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(UsuarioLblPais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(UsuarioTxtPais)
                    .addComponent(UsuarioTxtCiudad)
                    .addComponent(UsuarioTxtMail)
                    .addComponent(UsuarioTxtNombre)
                    .addComponent(UsuarioTxtApellido)
                    .addComponent(UsuarioTxtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblTel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtTel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblMail, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtMail, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblPais, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioTxtPais, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(UsuarioCBPadrino, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        UsuarioComboUsuarios.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioComboUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioComboUsuariosActionPerformed(evt);
            }
        });

        UsuarioLblUsuarios.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        UsuarioLblUsuarios.setForeground(new java.awt.Color(0, 102, 102));
        UsuarioLblUsuarios.setText("Usuarios");

        UsuarioLstFechas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioScrollFechas.setViewportView(UsuarioLstFechas);

        UsuarioLstActividades.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioScrollActividades.setViewportView(UsuarioLstActividades);

        UsuarioLblActividad.setText("Actividad:");

        UsuarioLblFecha.setText("Fecha");

        UsuarioLblActividades.setText("Próximas Actividades");

        javax.swing.GroupLayout usuarioPanActividadesLayout = new javax.swing.GroupLayout(usuarioPanActividades);
        usuarioPanActividades.setLayout(usuarioPanActividadesLayout);
        usuarioPanActividadesLayout.setHorizontalGroup(
            usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanActividadesLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(UsuarioLblActividades, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(usuarioPanActividadesLayout.createSequentialGroup()
                        .addGroup(usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(UsuarioScrollActividades, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioLblActividad))
                        .addGap(18, 18, 18)
                        .addGroup(usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(UsuarioLblFecha)
                            .addComponent(UsuarioScrollFechas, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(22, 22, 22))
        );
        usuarioPanActividadesLayout.setVerticalGroup(
            usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanActividadesLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(UsuarioLblActividades)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblActividad)
                    .addComponent(UsuarioLblFecha))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(usuarioPanActividadesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(UsuarioScrollActividades, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(UsuarioScrollFechas, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(usuarioPanActividades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 214, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(usuarioPanActividades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel3.setMaximumSize(new java.awt.Dimension(32767, 18267));
        jPanel3.setMinimumSize(new java.awt.Dimension(100, 182));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 789, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        UsuarioLblValor.setText("Valor:");

        UsuarioLblMoneda.setText("Moneda:");

        UsuarioLblPeriodo.setText("Periodicidad:");

        UsuarioLblMedio.setText("Medio:");

        UsuarioTxtValor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioTxtValor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#,##0.00"))));

        UsuarioComboMoneda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        UsuarioComboMoneda.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioComboPeriodo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        UsuarioComboPeriodo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioComboMedio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        UsuarioComboMedio.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        UsuarioLstAnimales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        UsuarioLstAnimales.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(UsuarioLstAnimales);

        UsuarioLblValor1.setText("Animales:");

        javax.swing.GroupLayout usuarioPanDepositoLayout = new javax.swing.GroupLayout(usuarioPanDeposito);
        usuarioPanDeposito.setLayout(usuarioPanDepositoLayout);
        usuarioPanDepositoLayout.setHorizontalGroup(
            usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(usuarioPanDepositoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, usuarioPanDepositoLayout.createSequentialGroup()
                        .addComponent(UsuarioLblPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(UsuarioComboPeriodo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(usuarioPanDepositoLayout.createSequentialGroup()
                        .addComponent(UsuarioLblValor, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(UsuarioTxtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(20, 20, 20)
                .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanDepositoLayout.createSequentialGroup()
                        .addComponent(UsuarioLblMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanDepositoLayout.createSequentialGroup()
                        .addComponent(UsuarioLblMedio, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(UsuarioComboMoneda, 0, 131, Short.MAX_VALUE)
                    .addComponent(UsuarioComboMedio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(49, 49, 49)
                .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(UsuarioLblValor1)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        usuarioPanDepositoLayout.setVerticalGroup(
            usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(usuarioPanDepositoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanDepositoLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UsuarioLblValor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioTxtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioLblMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioComboMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(usuarioPanDepositoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(UsuarioLblPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioComboPeriodo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioLblMedio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(UsuarioComboMedio, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, usuarioPanDepositoLayout.createSequentialGroup()
                        .addComponent(UsuarioLblValor1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        UsuarioBtnEliminar.setText("Eliminar");
        UsuarioBtnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioBtnEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(UsuarioLblUsuarios)
                        .addGap(18, 18, 18)
                        .addComponent(UsuarioComboUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(UsuarioBtnEliminar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(usuarioPanDeposito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 28, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UsuarioLblUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioComboUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuarioBtnEliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usuarioPanDeposito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout panUsuariosLayout = new javax.swing.GroupLayout(panUsuarios);
        panUsuarios.setLayout(panUsuariosLayout);
        panUsuariosLayout.setHorizontalGroup(
            panUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panUsuariosLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(panUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panUsuariosLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panUsuariosLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        panUsuariosLayout.setVerticalGroup(
            panUsuariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panUsuariosLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void UsuarioComboUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioComboUsuariosActionPerformed
        ocultarAgregarUsuario();
        if (UsuarioComboUsuarios.getItemCount() > 0 && !UsuarioComboUsuarios.getSelectedItem().toString().isEmpty()) {
            Integer id = Integer.parseInt(UsuarioComboUsuarios.getSelectedItem().toString().split(" - ")[0]);
            Usuario usuarioSeleccionado = sistema.buscarUsuarioPorId(id);
            UsuarioLblNombre.setText("Nombre: " + usuarioSeleccionado.getNombre());
            UsuarioLblApellido.setText("Apellido: " + usuarioSeleccionado.getApellido());
            UsuarioLblTel.setText("Telefóno: " + usuarioSeleccionado.getTelefono());
            UsuarioLblMail.setText("Mail: " + usuarioSeleccionado.getMail());
            UsuarioLblCiudad.setText("Ciudad: " + usuarioSeleccionado.getCiudad());
            UsuarioLblPais.setText("País: " + usuarioSeleccionado.getPais());
            String[] arrayActividades = new String[5];
            String[] arrayFechas = new String[5];
            int cantidadActividades = usuarioSeleccionado.getActividades().size();
            for (int i = 0; i < cantidadActividades; i++) {
                arrayActividades[i] = usuarioSeleccionado.getActividades().get(i).getNombre();
                Fecha fechaActividad = usuarioSeleccionado.getActividades().get(i).getFecha();
                arrayFechas[i] = fechaActividad.getDia() + "/" + fechaActividad.getMes() + "/" + fechaActividad.getAno();
            }
            UsuarioLstActividades.setListData(arrayActividades);
            UsuarioLstFechas.setListData(arrayFechas);

            mostrarDeposito(usuarioSeleccionado);
        } else {
            UsuarioLblNombre.setText("Nombre: ");
            UsuarioLblMail.setText("Mail: ");
            String[] arrayVacio = new String[0];
            UsuarioLstActividades.setListData(arrayVacio);
            UsuarioLstFechas.setListData(arrayVacio);
        }
    }//GEN-LAST:event_UsuarioComboUsuariosActionPerformed

    private void UsuarioBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioBtnAgregarActionPerformed
        if (agregarUsuarioSeleccionado == false) {
            mostrarAgregarUsuario();
        } else {
            if (UsuarioTxtNombre.getText().trim().equals("") || UsuarioTxtNombre.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Porfavor, ingrese un nombre para el usuario.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
            } else if (UsuarioTxtMail.getText().trim().equals("") || UsuarioTxtMail.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Porfavor, ingrese un email para el usuario.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
            } else if (!isValidEmailAddress(UsuarioTxtMail.getText())) {
                JOptionPane.showMessageDialog(this, "Porfavor, ingrese un email correcto para el usuario.", "Campo obligatorio incorrecto", JOptionPane.WARNING_MESSAGE);
            } else if (UsuarioCBPadrino.isSelected() && (UsuarioTxtValor.getText().trim().equals("") || UsuarioTxtValor.getText().equals("0.00"))) {
                JOptionPane.showMessageDialog(this, "Porfavor, ingrese un valor para el deposito del usuario, mayor a 0.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
            } else {
                Usuario usuarioAgregar = new Usuario();
                usuarioAgregar.setNombre(UsuarioTxtNombre.getText());
                usuarioAgregar.setApellido(UsuarioTxtApellido.getText());
                usuarioAgregar.setTelefono(UsuarioTxtTel.getText());
                usuarioAgregar.setMail(UsuarioTxtMail.getText());
                usuarioAgregar.setCiudad(UsuarioTxtCiudad.getText());
                usuarioAgregar.setPais(UsuarioTxtPais.getText());
                if (UsuarioCBPadrino.isSelected()) {
                    ArrayList<Animal> animalesApadrinados = new ArrayList();
                    for (Object obj : UsuarioLstAnimales.getSelectedValuesList()) {
                        Integer id = Integer.parseInt(obj.toString().split(" - ")[0]);
                        Animal animal = sistema.buscarAnimalPorId(id);
                        animalesApadrinados.add(animal);
                    }
                    usuarioAgregar.setAnimalesApadrinados(animalesApadrinados);
                    usuarioAgregar.setDeposito(UsuarioTxtValor.getText());
                    usuarioAgregar.setMoneda(UsuarioComboMoneda.getSelectedItem().toString());
                    usuarioAgregar.setPeriodo(UsuarioComboPeriodo.getSelectedItem().toString());
                    usuarioAgregar.setMedio(UsuarioComboMedio.getSelectedItem().toString());
                    usuarioAgregar.setEsPadrino(true);
                }
                sistema.AnadirUsuario(usuarioAgregar);
                UsuarioTxtNombre.setText("");
                UsuarioTxtApellido.setText("");
                UsuarioTxtTel.setText("");
                UsuarioTxtMail.setText("");
                UsuarioTxtCiudad.setText("");
                UsuarioTxtPais.setText("");
                ocultarAgregarUsuario();
                resetearListaUsuarios();
            }
        }
    }//GEN-LAST:event_UsuarioBtnAgregarActionPerformed

    private void UsuarioCBPadrinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioCBPadrinoActionPerformed
        mostrarAgregarDeposito();
    }//GEN-LAST:event_UsuarioCBPadrinoActionPerformed

    private void UsuarioTxtMailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioTxtMailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsuarioTxtMailActionPerformed

    private void UsuarioTxtTelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioTxtTelActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsuarioTxtTelActionPerformed

    private void UsuarioBtnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioBtnEliminarActionPerformed
        if(UsuarioComboUsuarios.getItemCount()>0){
            Usuario usuarioAEliminar = sistema.buscarUsuarioPorNombre((UsuarioComboUsuarios.getSelectedItem().toString().split(" - ")[1]).split(" ")[0]);
            if(usuarioAEliminar!=null){
                if(JOptionPane.showConfirmDialog(null, "Confirme que desea eliminar a un usuario seleccionada haciendo click en el boton si o yes.", "Se eliminara a "+usuarioAEliminar.getNombre(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                    sistema.EliminarUsuario(usuarioAEliminar);
                }
            }else{
                JOptionPane.showConfirmDialog(null, "Se produjo un error al eliminar a este usuario.", "Bug del sistema", JOptionPane.OK_OPTION);
            }
        }
        resetearListaUsuarios();
    }//GEN-LAST:event_UsuarioBtnEliminarActionPerformed

    private void ocultarAgregarUsuario() {
        UsuarioTxtNombre.setVisible(false);
        UsuarioTxtApellido.setVisible(false);
        UsuarioTxtTel.setVisible(false);
        UsuarioTxtMail.setVisible(false);
        UsuarioTxtCiudad.setVisible(false);
        UsuarioTxtPais.setVisible(false);
        UsuarioCBPadrino.setVisible(false);
        usuarioPanDeposito.setVisible(false);
        agregarUsuarioSeleccionado = false;
        usuarioPanActividades.setVisible(true);
    }

    private void resetearListaUsuarios() {
        if (UsuarioComboUsuarios.getItemCount() > 0) {
            UsuarioComboUsuarios.removeAllItems();
        }
        ArrayList<Usuario> usuarios = sistema.getUsuarios();
        UsuarioComboUsuarios.setModel(new DefaultComboBoxModel(usuarios.toArray()));
    }

    private void setearListaAnimalesApadrinar(ArrayList<Animal> animales) {
        DefaultComboBoxModel model = new DefaultComboBoxModel(animales.toArray());
        UsuarioLstAnimales.setModel(model);
    }

    private void setearListaMonedas() {
        UsuarioComboMoneda.removeAllItems();
        UsuarioComboMoneda.addItem("USD");
        UsuarioComboMoneda.addItem("UYU");
        UsuarioComboMoneda.addItem("ARS");
    }

    private void setearListaPeriodos() {
        UsuarioComboPeriodo.removeAllItems();
        UsuarioComboPeriodo.addItem("Mensual");
        UsuarioComboPeriodo.addItem("Trimestral");
        UsuarioComboPeriodo.addItem("Anual");
    }

    private void setearListaMedios() {
        UsuarioComboMedio.removeAllItems();
        UsuarioComboMedio.addItem("Tarjeta de crédito");
        UsuarioComboMedio.addItem("Transferencia bancaria");
    }
    
    private void mostrarDeposito(Usuario usuario) {
        if (usuario.getEspadrino()) {
            UsuarioCBPadrino.setVisible(true);
            UsuarioCBPadrino.setEnabled(false);
            usuarioPanDeposito.setVisible(true);
            UsuarioTxtValor.setVisible(false);
            UsuarioComboMoneda.setVisible(false);
            UsuarioComboPeriodo.setVisible(false);
            UsuarioComboMedio.setVisible(false);
            UsuarioLstAnimales.setEnabled(false);
            ArrayList<Animal> animales = usuario.getAnimalesApadrinados();
            setearListaAnimalesApadrinar(animales);
            UsuarioLblValor.setText("Valor: " + usuario.getDeposito());
            UsuarioLblMoneda.setText("Moneda: " + usuario.getMoneda());
            UsuarioLblPeriodo.setText("Periodo: " + usuario.getPeriodo());
            UsuarioLblMedio.setText("Medio: " + usuario.getMedio());
        }
    }
    
    private void mostrarAgregarDeposito(){
        usuarioPanDeposito.setVisible(UsuarioCBPadrino.isSelected());
        if (UsuarioCBPadrino.isSelected()) {
            UsuarioLblValor.setText("Valor:");
            UsuarioLblMoneda.setText("Moneda:");
            UsuarioLblPeriodo.setText("Periodo:");
            UsuarioLblMedio.setText("Medio:");
            UsuarioTxtValor.setVisible(true);
            UsuarioComboMoneda.setVisible(true);
            UsuarioComboPeriodo.setVisible(true);
            UsuarioComboMedio.setVisible(true);
            UsuarioLstAnimales.setEnabled(true);
            UsuarioTxtValor.setText("");
            ArrayList<Animal> animales = sistema.getAnimales();
            setearListaAnimalesApadrinar(animales);
            setearListaMonedas();
            setearListaPeriodos();
            setearListaMedios();
        }
    }
    
    private void mostrarAgregarUsuario() {
        agregarUsuarioSeleccionado = true;
        UsuarioLblNombre.setText("Nombre:");
        UsuarioLblApellido.setText("Apellido:");
        UsuarioLblTel.setText("Teléfono:");
        UsuarioLblMail.setText("Mail:");
        UsuarioLblCiudad.setText("Ciudad:");
        UsuarioLblPais.setText("País:");

        UsuarioTxtNombre.setVisible(true);
        UsuarioTxtApellido.setVisible(true);
        UsuarioTxtTel.setVisible(true);
        UsuarioTxtMail.setVisible(true);
        UsuarioTxtCiudad.setVisible(true);
        UsuarioTxtPais.setVisible(true);
        UsuarioCBPadrino.setVisible(true);
        UsuarioCBPadrino.setEnabled(true);
        UsuarioCBPadrino.setSelected(false);
        usuarioPanDeposito.setVisible(false);
        usuarioPanActividades.setVisible(false);
    }
    
    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
    
    public void inicializarPestanaUsuarios() {
        resetearListaUsuarios();
        if (UsuarioComboUsuarios.getItemCount() != 0 && !UsuarioComboUsuarios.getSelectedItem().toString().isEmpty()) {
            Integer id = Integer.parseInt(UsuarioComboUsuarios.getSelectedItem().toString().split(" - ")[0]);
            Usuario usuarioSeleccionado = sistema.buscarUsuarioPorId(id);
            UsuarioLblNombre.setText("Nombre: " + usuarioSeleccionado.getNombre());
            UsuarioLblApellido.setText("Apellido: " + usuarioSeleccionado.getApellido());
            UsuarioLblTel.setText("Teléfono: " + usuarioSeleccionado.getTelefono());
            UsuarioLblMail.setText("Mail: " + usuarioSeleccionado.getMail());
            UsuarioLblCiudad.setText("Ciudad: " + usuarioSeleccionado.getCiudad());
            UsuarioLblPais.setText("País: " + usuarioSeleccionado.getPais());

            if (usuarioSeleccionado.getEspadrino()) {
                UsuarioCBPadrino.setVisible(true);
                usuarioPanDeposito.setVisible(true);
            }
        } else {
            UsuarioLblNombre.setText("Nombre: ");
            UsuarioLblMail.setText("Mail: ");
        }
        ocultarAgregarUsuario();
        agregarUsuarioSeleccionado = false;

    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton UsuarioBtnAgregar;
    private javax.swing.JButton UsuarioBtnEliminar;
    private javax.swing.JCheckBox UsuarioCBPadrino;
    private javax.swing.JComboBox<String> UsuarioComboMedio;
    private javax.swing.JComboBox<String> UsuarioComboMoneda;
    private javax.swing.JComboBox<String> UsuarioComboPeriodo;
    private javax.swing.JComboBox<String> UsuarioComboUsuarios;
    private javax.swing.JLabel UsuarioLblActividad;
    private javax.swing.JLabel UsuarioLblActividades;
    private javax.swing.JLabel UsuarioLblApellido;
    private javax.swing.JLabel UsuarioLblCiudad;
    private javax.swing.JLabel UsuarioLblFecha;
    private javax.swing.JLabel UsuarioLblMail;
    private javax.swing.JLabel UsuarioLblMedio;
    private javax.swing.JLabel UsuarioLblMoneda;
    private javax.swing.JLabel UsuarioLblNombre;
    private javax.swing.JLabel UsuarioLblPais;
    private javax.swing.JLabel UsuarioLblPeriodo;
    private javax.swing.JLabel UsuarioLblTel;
    private javax.swing.JLabel UsuarioLblUsuarios;
    private javax.swing.JLabel UsuarioLblValor;
    private javax.swing.JLabel UsuarioLblValor1;
    private javax.swing.JList<String> UsuarioLstActividades;
    private javax.swing.JList<String> UsuarioLstAnimales;
    private javax.swing.JList<String> UsuarioLstFechas;
    private javax.swing.JScrollPane UsuarioScrollActividades;
    private javax.swing.JScrollPane UsuarioScrollFechas;
    private javax.swing.JTextField UsuarioTxtApellido;
    private javax.swing.JTextField UsuarioTxtCiudad;
    private javax.swing.JTextField UsuarioTxtMail;
    private javax.swing.JTextField UsuarioTxtNombre;
    private javax.swing.JTextField UsuarioTxtPais;
    private javax.swing.JFormattedTextField UsuarioTxtTel;
    private javax.swing.JFormattedTextField UsuarioTxtValor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panUsuarios;
    private javax.swing.JPanel usuarioPanActividades;
    private javax.swing.JPanel usuarioPanDeposito;
    // End of variables declaration//GEN-END:variables
}
