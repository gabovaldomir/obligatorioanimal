package interfaz;

import dominio.Sistema;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * @author gabomir
 * @author natagarc
 */
public class VentanaPrincipal extends javax.swing.JFrame {
    private Calendario panelCalendario;
    private Animales panelAnimales;
    private Usuarios panelUsuarios;
    private Adopciones panelAdopciones;
    private Sistema sistema;

    public VentanaPrincipal(Sistema sis) {
        sistema = sis;
        initComponents();
        inicializacionComponentes(sis);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        pack();
    }
    
    //POS: Inicializa componentes de ventana al Sistema sis
    private void inicializacionComponentes(Sistema sis){
        panelAnimales = new Animales(sis);
        panelAnimales.setBounds(0, 0, 900, 930);
        panelAnimales.setPreferredSize(new Dimension(300, 300));
        panelMain.add(panelAnimales);
        panelUsuarios = new Usuarios(sis);
        panelUsuarios.setBounds(0, 0, 900, 930);
        panelUsuarios.setPreferredSize(new Dimension(300, 300));
        panelMain.add(panelUsuarios);
        panelAdopciones = new Adopciones(sis);
        panelAdopciones.setBounds(0, 0, 900, 930);
        panelAdopciones.setPreferredSize(new Dimension(300, 300));
        panelMain.add(panelAdopciones);
        panelCalendario = new Calendario(sis);
        panelCalendario.setBounds(0, 0, 1100, 930);
        panelCalendario.setPreferredSize(new Dimension(300, 300));
        panelMain.add(panelCalendario);
        panelCalendario.setVisible(true);
        panelAnimales.setVisible(false);  
        panelUsuarios.setVisible(false);  
        panelAdopciones.setVisible(false); 
        CalendarioBtn.setContentAreaFilled(false);
        CalendarioBtn.setBorderPainted(false);
        UsuariosBtn.setContentAreaFilled(false);
        UsuariosBtn.setBorderPainted(false);
        AnimalesBtn.setContentAreaFilled(false);
        AnimalesBtn.setBorderPainted(false);
        AdopcionesBtn.setContentAreaFilled(false);
        AdopcionesBtn.setBorderPainted(false);
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario2.png")));
        JFrame.setDefaultLookAndFeelDecorated(true);
    }
    
    private void mostrarCalendario(){
        panelCalendario.setVisible(true);
        panelAnimales.setVisible(false);  
        panelUsuarios.setVisible(false);  
        panelAdopciones.setVisible(false);  
        panelCalendario.resetearListaUsuarios();
        panelCalendario.resetearListaAnimales();
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario2.png")));
        UsuariosBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios1.png")));
        AnimalesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales1.png")));
        AdopcionesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones1.png")));
    }
    
    private void mostrarAnimales(){
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario1.png")));
        UsuariosBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios1.png")));
        AnimalesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales2.png")));
        AdopcionesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones1.png")));
        panelCalendario.setVisible(false);
        panelAnimales.setVisible(true);  
        panelUsuarios.setVisible(false);  
        panelAdopciones.setVisible(false);  
        panelAnimales.resetearPestanaAnimales();
    }
    
    private void mostrarUsuarios(){
        panelCalendario.setVisible(false);
        panelAnimales.setVisible(false);  
        panelUsuarios.setVisible(true);  
        panelAdopciones.setVisible(false);  
        panelUsuarios.inicializarPestanaUsuarios();
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario1.png")));
        UsuariosBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios2.png")));
        AnimalesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales1.png")));
        AdopcionesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones1.png")));
    }
    
    private void mostrarAdopciones(){
        panelCalendario.setVisible(false);
        panelAnimales.setVisible(false);  
        panelUsuarios.setVisible(false);  
        panelAdopciones.setVisible(true); 
        panelAdopciones.mostrarAgregarAdopcion();
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario1.png")));
        UsuariosBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios1.png")));
        AnimalesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales1.png")));
        AdopcionesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones2.png")));
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelMain = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        CalendarioBtn = new javax.swing.JButton();
        UsuariosBtn = new javax.swing.JButton();
        AnimalesBtn = new javax.swing.JButton();
        AdopcionesBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Animales Sin Hogar");
        setIconImage(new ImageIcon(getClass().getResource("images/icon.png")).getImage());

        jPanel1.setPreferredSize(new java.awt.Dimension(1070, 633));

        panelMain.setMinimumSize(new java.awt.Dimension(1070, 621));
        panelMain.setPreferredSize(new java.awt.Dimension(1070, 621));

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 640, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, 1081, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 215, 64));

        CalendarioBtn.setBackground(new java.awt.Color(255, 215, 64));
        CalendarioBtn.setForeground(new java.awt.Color(255, 215, 64));
        CalendarioBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario1.png"))); // NOI18N
        CalendarioBtn.setBorderPainted(false);
        CalendarioBtn.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario3.png"))); // NOI18N
        CalendarioBtn.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_calendario2.png"))); // NOI18N
        CalendarioBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalendarioBtnActionPerformed(evt);
            }
        });

        UsuariosBtn.setBackground(new java.awt.Color(255, 215, 64));
        UsuariosBtn.setForeground(new java.awt.Color(255, 215, 64));
        UsuariosBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios1.png"))); // NOI18N
        UsuariosBtn.setBorderPainted(false);
        UsuariosBtn.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios3.png"))); // NOI18N
        UsuariosBtn.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_usuarios2.png"))); // NOI18N
        UsuariosBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuariosBtnActionPerformed(evt);
            }
        });

        AnimalesBtn.setBackground(new java.awt.Color(255, 215, 64));
        AnimalesBtn.setForeground(new java.awt.Color(255, 215, 64));
        AnimalesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales1.png"))); // NOI18N
        AnimalesBtn.setToolTipText("");
        AnimalesBtn.setBorderPainted(false);
        AnimalesBtn.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales3.png"))); // NOI18N
        AnimalesBtn.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_animales2.png"))); // NOI18N
        AnimalesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalesBtnActionPerformed(evt);
            }
        });

        AdopcionesBtn.setBackground(new java.awt.Color(255, 215, 64));
        AdopcionesBtn.setForeground(new java.awt.Color(255, 215, 64));
        AdopcionesBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones1.png"))); // NOI18N
        AdopcionesBtn.setBorderPainted(false);
        AdopcionesBtn.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones3.png"))); // NOI18N
        AdopcionesBtn.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/btn_adopciones2.png"))); // NOI18N
        AdopcionesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AdopcionesBtnActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(255, 215, 64));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/interfaz/images/pets.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(CalendarioBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(UsuariosBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(AnimalesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AdopcionesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalendarioBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AnimalesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AdopcionesBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(UsuariosBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1081, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 593, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 63, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CalendarioBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalendarioBtnActionPerformed
        mostrarCalendario();
    }//GEN-LAST:event_CalendarioBtnActionPerformed

    private void AnimalesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalesBtnActionPerformed
        mostrarAnimales();
    }//GEN-LAST:event_AnimalesBtnActionPerformed

    private void UsuariosBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuariosBtnActionPerformed
        mostrarUsuarios();
    }//GEN-LAST:event_UsuariosBtnActionPerformed

    private void AdopcionesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AdopcionesBtnActionPerformed
        mostrarAdopciones();
    }//GEN-LAST:event_AdopcionesBtnActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AdopcionesBtn;
    private javax.swing.JButton AnimalesBtn;
    private javax.swing.JButton CalendarioBtn;
    private javax.swing.JButton UsuariosBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel panelMain;
    // End of variables declaration//GEN-END:variables
}
