package interfaz;

import dominio.*;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * @author natagarc
 * @author gabomir
 */
public class Adopciones extends javax.swing.JPanel {
    private Sistema sistema;

    public Adopciones(Sistema sis) {
        sistema = sis;
        initComponents();
        mostrarAgregarAdopcion();
        this.setSize(950, 625);
    }
    
    public void mostrarAgregarAdopcion(){
        List<Animal> listAnimales = sistema.getAnimalesAdoptables();
        List<Usuario> listUsuarios = sistema.getUsuarios();
        adopcionLstAnimales.removeAll();
        adopcionLstTutores.removeAll();
        DefaultComboBoxModel modelAnimales = new DefaultComboBoxModel(listAnimales.toArray());
        adopcionLstAnimales.setModel(modelAnimales);
        DefaultComboBoxModel modelUsuarios = new DefaultComboBoxModel(listUsuarios.toArray());
        adopcionLstTutores.setModel(modelUsuarios);
        adopcionesLblAdvertencia.setText("");
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panAdopciones = new javax.swing.JPanel();
        lblAdopciones = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        adopcionLstTutores = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        adopcionLstAnimales = new javax.swing.JList<>();
        adopcionLblTutores = new javax.swing.JLabel();
        adopcionLblAnimales = new javax.swing.JLabel();
        btnRealizarAdopcion = new javax.swing.JButton();
        adopcionesLblAdvertencia = new javax.swing.JLabel();

        setMaximumSize(new java.awt.Dimension(833, 572));
        setMinimumSize(new java.awt.Dimension(833, 572));
        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(833, 572));

        panAdopciones.setMaximumSize(new java.awt.Dimension(833, 572));
        panAdopciones.setMinimumSize(new java.awt.Dimension(833, 572));
        panAdopciones.setOpaque(false);
        panAdopciones.setPreferredSize(new java.awt.Dimension(833, 572));

        lblAdopciones.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblAdopciones.setForeground(new java.awt.Color(0, 102, 102));
        lblAdopciones.setText("Adopciones");

        adopcionLstTutores.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        adopcionLstTutores.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(adopcionLstTutores);

        adopcionLstAnimales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        adopcionLstAnimales.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(adopcionLstAnimales);

        adopcionLblTutores.setText("Tutores");

        adopcionLblAnimales.setText("Animales");

        btnRealizarAdopcion.setLabel("Realizar Adopción");
        btnRealizarAdopcion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRealizarAdopcionMouseClicked(evt);
            }
        });
        btnRealizarAdopcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRealizarAdopcionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panAdopcionesLayout = new javax.swing.GroupLayout(panAdopciones);
        panAdopciones.setLayout(panAdopcionesLayout);
        panAdopcionesLayout.setHorizontalGroup(
            panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAdopcionesLayout.createSequentialGroup()
                .addGroup(panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panAdopcionesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblAdopciones))
                    .addGroup(panAdopcionesLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnRealizarAdopcion, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(adopcionesLblAdvertencia, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panAdopcionesLayout.createSequentialGroup()
                .addContainerGap(191, Short.MAX_VALUE)
                .addGroup(panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(adopcionLblTutores))
                .addGap(126, 126, 126)
                .addGroup(panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(adopcionLblAnimales)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(102, 102, 102))
        );
        panAdopcionesLayout.setVerticalGroup(
            panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAdopcionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAdopciones)
                .addGap(29, 29, 29)
                .addGroup(panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panAdopcionesLayout.createSequentialGroup()
                        .addComponent(adopcionLblTutores)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panAdopcionesLayout.createSequentialGroup()
                        .addComponent(adopcionLblAnimales)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(146, 146, 146)
                .addGroup(panAdopcionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnRealizarAdopcion, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(adopcionesLblAdvertencia, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panAdopciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panAdopciones, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRealizarAdopcionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRealizarAdopcionMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnRealizarAdopcionMouseClicked

    private void btnRealizarAdopcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRealizarAdopcionActionPerformed
        ArrayList<Usuario> tutorSeleccionado = new ArrayList();
        ArrayList<Animal> listAnimalesSeleccionados = new ArrayList();
        int cantidadTutoresSeleccionados = adopcionLstTutores.getSelectedValuesList().size();
        int cantidadAnimalesSeleccionados = adopcionLstAnimales.getSelectedValuesList().size();
        if(cantidadTutoresSeleccionados!=1)adopcionesLblAdvertencia.setText("Debe seleccionar solamente a 1 tutor.");
        else{
            if(cantidadAnimalesSeleccionados<1)adopcionesLblAdvertencia.setText("Debe seleccionar al menos 1 animal a adoptar.");
            else{
                for(Object obj : adopcionLstTutores.getSelectedValuesList()){
                    int id = Integer.parseInt(obj.toString().split(" - ")[0]);
                    Usuario tutor = sistema.buscarUsuarioPorId(id);
                    tutorSeleccionado.add(tutor);
                }
                for(Object obj : adopcionLstAnimales.getSelectedValuesList()){
                    int id = Integer.parseInt(obj.toString().split(" - ")[0]);
                    Animal animal = sistema.buscarAnimalPorId(id);
                    listAnimalesSeleccionados.add(animal);
                }
                for(Animal animal : listAnimalesSeleccionados){
                    animal.setTutor(tutorSeleccionado.get(0));
                }
                JOptionPane.showConfirmDialog(this, tutorSeleccionado.get(0).getNombre()+" ha adoptado correctamente.", "Adopcion Realizada", JOptionPane.DEFAULT_OPTION,1);
                mostrarAgregarAdopcion();
            }
        }
    }//GEN-LAST:event_btnRealizarAdopcionActionPerformed
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel adopcionLblAnimales;
    private javax.swing.JLabel adopcionLblTutores;
    private javax.swing.JList<String> adopcionLstAnimales;
    private javax.swing.JList<String> adopcionLstTutores;
    private javax.swing.JLabel adopcionesLblAdvertencia;
    private javax.swing.JButton btnRealizarAdopcion;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblAdopciones;
    private javax.swing.JPanel panAdopciones;
    // End of variables declaration//GEN-END:variables
}
