package interfaz;

import dominio.*;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * @author gabomir
 * @author natagarc
 */
public class Animales extends javax.swing.JPanel {
    private Sistema sistema;
    private boolean agregarAnimalSeleccionado;
    private String rutaImagenAgregar;
    private String rutaImagenRuta;
    
    public Animales(Sistema sis) {
        sistema = sis;
        initComponents();
        try {
            AnimalLblFoto.setIcon(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_default.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        AnimalBtnEditar.setVisible(false);
        AnimalBtnGuardar.setVisible(false);
        rutaImagenAgregar = "";
        rutaImagenRuta = "";
        agregarAnimalSeleccionado = true;
        this.setSize(950, 625);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panAnimales = new javax.swing.JPanel();
        AnimalLblAnimales = new javax.swing.JLabel();
        AnimalComboAnimales = new javax.swing.JComboBox<>();
        AnimalBtnEditar = new javax.swing.JButton();
        AnimalPanInformacion = new javax.swing.JPanel();
        AnimalLblAltura = new javax.swing.JLabel();
        AnimalLblComentarios = new javax.swing.JLabel();
        AnimalLblNombre = new javax.swing.JLabel();
        AnimalLblPeso = new javax.swing.JLabel();
        AnimalTxtNombre = new javax.swing.JTextField();
        AnimalSpinAltura = new javax.swing.JSpinner();
        AnimalSpinPeso = new javax.swing.JSpinner();
        AnimalBtnExaminar = new javax.swing.JButton();
        AnimalChkBxAdoptable = new javax.swing.JCheckBox();
        AnimalTxtComentarios = new javax.swing.JTextField();
        AnimalLblFotoBoton = new javax.swing.JLabel();
        CalLblCentimetros = new javax.swing.JLabel();
        CalLblKilogramos = new javax.swing.JLabel();
        AnimalLblTutor = new javax.swing.JLabel();
        AnimalLblPadrino = new javax.swing.JLabel();
        AnimalLblAdvertencia = new javax.swing.JLabel();
        AnimalPanFoto = new javax.swing.JPanel();
        AnimalLblFoto = new javax.swing.JLabel();
        AnimalBtnGuardar = new javax.swing.JButton();
        AnimalBtnAgregar = new javax.swing.JButton();
        AnimalBtnEliminar = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(32767, 572));
        setMinimumSize(new java.awt.Dimension(0, 572));
        setPreferredSize(new java.awt.Dimension(883, 572));

        panAnimales.setPreferredSize(new java.awt.Dimension(883, 633));
        panAnimales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                panAnimalesMouseClicked(evt);
            }
        });

        AnimalLblAnimales.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        AnimalLblAnimales.setForeground(new java.awt.Color(0, 102, 102));
        AnimalLblAnimales.setText("Animales");

        AnimalComboAnimales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        AnimalComboAnimales.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                AnimalComboAnimalesMouseClicked(evt);
            }
        });
        AnimalComboAnimales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalComboAnimalesActionPerformed(evt);
            }
        });

        AnimalBtnEditar.setText("Editar");
        AnimalBtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalBtnEditarActionPerformed(evt);
            }
        });

        AnimalLblAltura.setText("Altura:");

        AnimalLblComentarios.setText("Comentarios:");

        AnimalLblNombre.setText("Nombre:");

        AnimalLblPeso.setText("Peso:");

        AnimalTxtNombre.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));
        AnimalTxtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalTxtNombreActionPerformed(evt);
            }
        });

        AnimalSpinAltura.setModel(new javax.swing.SpinnerNumberModel(0, 0, null, 1));
        AnimalSpinAltura.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));

        AnimalSpinPeso.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, null, 0.1d));
        AnimalSpinPeso.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));

        AnimalBtnExaminar.setText("Examinar");
        AnimalBtnExaminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalBtnExaminarActionPerformed(evt);
            }
        });

        AnimalChkBxAdoptable.setText("Adoptable");
        AnimalChkBxAdoptable.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));
        AnimalChkBxAdoptable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalChkBxAdoptableActionPerformed(evt);
            }
        });

        AnimalTxtComentarios.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));

        AnimalLblFotoBoton.setText("Foto:");

        CalLblCentimetros.setText("Centímetros");

        CalLblKilogramos.setText("Kilogramos");

        javax.swing.GroupLayout AnimalPanFotoLayout = new javax.swing.GroupLayout(AnimalPanFoto);
        AnimalPanFoto.setLayout(AnimalPanFotoLayout);
        AnimalPanFotoLayout.setHorizontalGroup(
            AnimalPanFotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AnimalPanFotoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AnimalLblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        AnimalPanFotoLayout.setVerticalGroup(
            AnimalPanFotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AnimalPanFotoLayout.createSequentialGroup()
                .addComponent(AnimalLblFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout AnimalPanInformacionLayout = new javax.swing.GroupLayout(AnimalPanInformacion);
        AnimalPanInformacion.setLayout(AnimalPanInformacionLayout);
        AnimalPanInformacionLayout.setHorizontalGroup(
            AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AnimalPanInformacionLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AnimalLblAdvertencia, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                        .addComponent(AnimalChkBxAdoptable)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(AnimalLblNombre)
                            .addComponent(AnimalLblAltura)
                            .addComponent(AnimalLblPeso)
                            .addComponent(AnimalLblFotoBoton))
                        .addGap(78, 78, 78)
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(AnimalBtnExaminar, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AnimalTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                                .addComponent(AnimalSpinAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CalLblCentimetros))
                            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                                .addComponent(AnimalSpinPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CalLblKilogramos)))
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 163, Short.MAX_VALUE)
                                .addComponent(AnimalPanFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                                .addGap(100, 100, 100)
                                .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(AnimalLblTutor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(AnimalLblPadrino, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, AnimalPanInformacionLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AnimalTxtComentarios, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(AnimalLblComentarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(402, 402, 402))
        );
        AnimalPanInformacionLayout.setVerticalGroup(
            AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(AnimalPanFoto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(AnimalLblPadrino, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(AnimalPanInformacionLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(AnimalLblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AnimalTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(AnimalSpinAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AnimalLblAltura, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblCentimetros))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(AnimalSpinPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AnimalLblPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblKilogramos))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(AnimalPanInformacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(AnimalBtnExaminar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(AnimalLblFotoBoton, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AnimalLblTutor, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(AnimalChkBxAdoptable)
                .addGap(18, 18, 18)
                .addComponent(AnimalLblComentarios, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(AnimalTxtComentarios, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AnimalLblAdvertencia, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        AnimalBtnGuardar.setText("Guardar");
        AnimalBtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalBtnGuardarActionPerformed(evt);
            }
        });

        AnimalBtnAgregar.setText("Ingresar Animal");
        AnimalBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalBtnAgregarActionPerformed(evt);
            }
        });

        AnimalBtnEliminar.setText("Eliminar");
        AnimalBtnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnimalBtnEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panAnimalesLayout = new javax.swing.GroupLayout(panAnimales);
        panAnimales.setLayout(panAnimalesLayout);
        panAnimalesLayout.setHorizontalGroup(
            panAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAnimalesLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(AnimalBtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(AnimalBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panAnimalesLayout.createSequentialGroup()
                .addGroup(panAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panAnimalesLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(AnimalLblAnimales)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(AnimalComboAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addComponent(AnimalBtnEditar)
                        .addGap(18, 18, 18)
                        .addComponent(AnimalBtnEliminar))
                    .addComponent(AnimalPanInformacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 201, Short.MAX_VALUE))
        );
        panAnimalesLayout.setVerticalGroup(
            panAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panAnimalesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AnimalLblAnimales)
                    .addComponent(AnimalComboAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AnimalBtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AnimalBtnEliminar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AnimalPanInformacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(111, 111, 111)
                .addGroup(panAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(AnimalBtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AnimalBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        AnimalBtnGuardar.getAccessibleContext().setAccessibleName("");
        AnimalBtnAgregar.getAccessibleContext().setAccessibleName("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panAnimales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panAnimales, javax.swing.GroupLayout.DEFAULT_SIZE, 617, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void panAnimalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panAnimalesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_panAnimalesMouseClicked

    private void AnimalBtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalBtnGuardarActionPerformed
        Animal animalSeleccionado = sistema.buscarAnimalPorNombre(AnimalComboAnimales.getSelectedItem().toString());
        String valor = AnimalSpinAltura.getValue() + "";
        double altura = Double.parseDouble(valor);
        animalSeleccionado.setAltura(altura);
        animalSeleccionado.setComentarios(AnimalTxtComentarios.getText());
        animalSeleccionado.setNombre(AnimalTxtNombre.getText());
        valor = AnimalSpinPeso.getValue() + "";
        double peso = Double.parseDouble(valor);
        animalSeleccionado.setPeso(peso);
        if (!rutaImagenAgregar.equals("")) {
            File imagen = new File(rutaImagenAgregar);
            animalSeleccionado.setFoto(crearIcono(imagen, 100));
            rutaImagenAgregar = "";
        }
        ocultarAgregarAnimal();
        int pos = AnimalComboAnimales.getSelectedIndex();
        setearListaAnimales();
        AnimalComboAnimales.setSelectedIndex(pos);
        AnimalChkBxAdoptable.setEnabled(false);
    }//GEN-LAST:event_AnimalBtnGuardarActionPerformed

    private void AnimalBtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalBtnEditarActionPerformed
        mostrarEditarAnimal();
    }//GEN-LAST:event_AnimalBtnEditarActionPerformed

    private void AnimalComboAnimalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalComboAnimalesActionPerformed
        ocultarAgregarAnimal();
        if (AnimalComboAnimales.getItemCount() > 0) {
            Animal animalSeleccionado = sistema.buscarAnimalPorNombre(AnimalComboAnimales.getSelectedItem().toString());
            AnimalLblNombre.setText(animalSeleccionado.getNombre().toUpperCase());
            AnimalLblPeso.setText("Pesa:       " + animalSeleccionado.getPeso());
            AnimalLblAltura.setText("Mide:       " + animalSeleccionado.getAltura());
            AnimalLblComentarios.setText("" + animalSeleccionado.getComentarios());
            AnimalLblFoto.setIcon(animalSeleccionado.getFoto());
            AnimalBtnGuardar.setVisible(false);
            AnimalBtnEditar.setVisible(true);
            AnimalBtnAgregar.setVisible(true);
            AnimalChkBxAdoptable.setSelected(animalSeleccionado.isAdoptable());
            if(animalSeleccionado.getTutor()!= null){
                AnimalLblTutor.setText("El tutor del animal es: "+animalSeleccionado.getTutor().getNombre()+".");
            }
            List<Usuario> listaUsuarios = sistema.getUsuarios();
            String padrinosDelAnimal = "";
            for(Usuario usuario : listaUsuarios){
                for(Animal animalApadrinado : usuario.getAnimalesApadrinados()){
                    if(animalSeleccionado.getId() == animalApadrinado.getId()){
                        if(padrinosDelAnimal.trim().equals("") || padrinosDelAnimal.isEmpty()){
                            padrinosDelAnimal = padrinosDelAnimal+usuario.getNombre();
                        }else{
                            padrinosDelAnimal = padrinosDelAnimal+", "+usuario.getNombre();
                        }
                    }
                }
            }
            if(!(padrinosDelAnimal.trim().equals("") || padrinosDelAnimal.isEmpty())){
                if(padrinosDelAnimal.split(",").length > 1){
                    padrinosDelAnimal = "Los padrinos del animal son: "+padrinosDelAnimal+".";
                }else{
                    padrinosDelAnimal = "El padrino del animal es: "+padrinosDelAnimal+".";
                }
            }
            System.out.println(padrinosDelAnimal);
            AnimalLblPadrino.setText(padrinosDelAnimal);
        } else {
            AnimalLblNombre.setText("Nombre: ");
            AnimalLblPeso.setText("Peso ");
            AnimalLblAltura.setText("Altura: ");
            AnimalLblComentarios.setText("Comentarios: ");
            try {
                AnimalLblFoto.setIcon(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_default.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }
    }//GEN-LAST:event_AnimalComboAnimalesActionPerformed

    private void AnimalComboAnimalesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_AnimalComboAnimalesMouseClicked

    }//GEN-LAST:event_AnimalComboAnimalesMouseClicked

    private void AnimalChkBxAdoptableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalChkBxAdoptableActionPerformed
        mostrarSeleccionarTutor();
    }//GEN-LAST:event_AnimalChkBxAdoptableActionPerformed

    private void AnimalBtnExaminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalBtnExaminarActionPerformed
        JFileChooser elegirImagen = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        elegirImagen.setFileFilter(filter);
        int result = elegirImagen.showDialog(this, "Elija una imagen");
        if (result == JFileChooser.APPROVE_OPTION) {
            File imgSeleccionada = elegirImagen.getSelectedFile();
            rutaImagenAgregar = imgSeleccionada.getAbsolutePath();
            AnimalLblFoto.setIcon(crearIcono(imgSeleccionada, 100));
        } else {
            AnimalLblAdvertencia.setText("No se ha podido ingresar la imágen de forma correcta");
        }
    }//GEN-LAST:event_AnimalBtnExaminarActionPerformed

    private void AnimalTxtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalTxtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AnimalTxtNombreActionPerformed

    private void AnimalBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalBtnAgregarActionPerformed
        if (agregarAnimalSeleccionado == false) {
            mostrarAgregarAnimal();
        } else {
            if (AnimalTxtNombre.getText().equals("")) {
                AnimalLblAdvertencia.setText("Por favor ingrese un nombre");
            } else if (AnimalSpinAltura.getValue().equals(0)) {
                AnimalLblAdvertencia.setText("Por favor ingrese una altura válida");
            } else if (AnimalSpinPeso.getValue().equals(0.0)) {
                AnimalLblAdvertencia.setText("Por favor ingrese un peso válido");
            } else {
                Animal animalAgregar = new Animal(AnimalTxtNombre.getText(), Integer.parseInt(AnimalSpinAltura.getValue().toString()), Double.parseDouble(AnimalSpinPeso.getValue().toString()), AnimalTxtComentarios.getText(), AnimalChkBxAdoptable.isSelected());
                if (rutaImagenAgregar.equals("")) {
                    try {
                        animalAgregar.setFoto(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_default.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                } else {
                    File imagen = new File(rutaImagenAgregar);
                    animalAgregar.setFoto(crearIcono(imagen, 100));
                    rutaImagenAgregar = "";
                }
                sistema.AnadirAnimal(animalAgregar);
                AnimalTxtNombre.setText("");
                AnimalSpinPeso.setValue((Object) 0.0);
                AnimalSpinAltura.setValue((Object) 0.0);
                AnimalTxtComentarios.setText("");
                AnimalLblAdvertencia.setText("");
                ocultarAgregarAnimal();
                setearListaAnimales();
                AnimalChkBxAdoptable.setEnabled(false);
            }
        }
    }//GEN-LAST:event_AnimalBtnAgregarActionPerformed

    private void AnimalBtnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnimalBtnEliminarActionPerformed
        if(AnimalComboAnimales.getItemCount()>0){
            Animal animalAEliminar = sistema.buscarAnimalPorNombre(AnimalComboAnimales.getSelectedItem().toString());
            if(JOptionPane.showConfirmDialog(null, "Confirme que desea eliminar a una mascota seleccionada haciendo click en el boton si o yes.", "Se eliminara a "+animalAEliminar.getNombre(), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                sistema.EliminarAnimal(animalAEliminar);
            }
        }
        resetearPestanaAnimales();
    }//GEN-LAST:event_AnimalBtnEliminarActionPerformed

    private void mostrarAgregarAnimal() {
        agregarAnimalSeleccionado = true;
        AnimalLblNombre.setText("Nombre:");
        AnimalLblPeso.setText("Peso:");
        AnimalLblAltura.setText("Altura:");
        AnimalLblComentarios.setText("Comentarios:");
        try {
            AnimalLblFoto.setIcon(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_default.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        AnimalTxtNombre.setVisible(true);
        AnimalSpinPeso.setVisible(true);
        AnimalSpinAltura.setVisible(true);
        AnimalTxtComentarios.setVisible(true);
        AnimalLblFotoBoton.setVisible(true);
        AnimalBtnExaminar.setVisible(true);
        AnimalChkBxAdoptable.setVisible(true);
    }

    private void ocultarAgregarAnimal() {
        AnimalTxtNombre.setVisible(false);
        AnimalTxtNombre.setText("");
        AnimalSpinPeso.setVisible(false);
        AnimalSpinPeso.setValue((Object) 0.0);
        AnimalSpinAltura.setVisible(false);
        AnimalSpinAltura.setValue((Object) 0.0);
        AnimalTxtComentarios.setVisible(false);
        AnimalTxtComentarios.setText("");
        AnimalLblFotoBoton.setVisible(false);
        AnimalBtnExaminar.setVisible(false);
        agregarAnimalSeleccionado = false;
        AnimalChkBxAdoptable.setEnabled(false);
        AnimalLblTutor.setEnabled(false);
    }

    private void mostrarEditarAnimal() {
        AnimalBtnGuardar.setVisible(true);
        AnimalBtnAgregar.setVisible(false);
        agregarAnimalSeleccionado = true;
        AnimalLblNombre.setText("Nombre:");
        AnimalLblPeso.setText("Peso:");
        AnimalLblAltura.setText("Altura:");
        AnimalLblComentarios.setText("Comentarios:");
        Animal animalSeleccionado = sistema.buscarAnimalPorNombre(AnimalComboAnimales.getSelectedItem().toString());
        AnimalLblFoto.setIcon(animalSeleccionado.getFoto());
        AnimalTxtNombre.setText(animalSeleccionado.getNombre());
        AnimalSpinPeso.setValue(animalSeleccionado.getPeso());
        AnimalSpinAltura.setValue(animalSeleccionado.getAltura());
        AnimalTxtComentarios.setText("" + animalSeleccionado.getComentarios());
        AnimalTxtNombre.setVisible(true);
        AnimalSpinPeso.setVisible(true);
        AnimalSpinAltura.setVisible(true);
        AnimalTxtComentarios.setVisible(true);
        AnimalLblFotoBoton.setVisible(true);
        AnimalBtnExaminar.setVisible(true);
        AnimalChkBxAdoptable.setEnabled(true);
        AnimalChkBxAdoptable.setSelected(animalSeleccionado.isAdoptable());
        AnimalLblTutor.setEnabled(true);
        if(animalSeleccionado.getTutor()!=null){
            AnimalLblTutor.setText(animalSeleccionado.getTutor().getNombre());
        }
    }
    
    private void setearListaAnimales() {
        if (AnimalComboAnimales.getItemCount() > 0) {
            AnimalComboAnimales.removeAllItems();
        }
        for (int i = 0; i < sistema.getAnimales().size(); i++) {
            AnimalComboAnimales.addItem(sistema.getAnimales().get(i).getNombre());
        }
    }

    private ImageIcon crearIcono(String ruta, int tamano) {
        ImageIcon retorno = null;
        try {
            retorno = new ImageIcon(ImageIO.read(getClass().getResource(ruta)).getScaledInstance(tamano, -1, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            System.out.println(e);
        }
        return retorno;
    }

    private ImageIcon crearIcono(File file, int tamano) {
        ImageIcon retorno = null;
        try {
            retorno = new ImageIcon(ImageIO.read(file).getScaledInstance(tamano, -1, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            System.out.println(e);
        }
        return retorno;
    }
    
    private void mostrarSeleccionarTutor(){
        if(AnimalChkBxAdoptable.isSelected()){
            AnimalLblTutor.setEnabled(true);
        }
    }
    private void cargarValoresGenericosVentana(){
        if (AnimalComboAnimales.getItemCount() != 0) {
            Animal animalSeleccionado = sistema.buscarAnimalPorNombre(AnimalComboAnimales.getSelectedItem().toString());
            AnimalLblNombre.setText(animalSeleccionado.getNombre().toUpperCase());
            AnimalLblPeso.setText("Pesa:       " + animalSeleccionado.getPeso());
            AnimalLblAltura.setText("Mide:       " + animalSeleccionado.getAltura());
            AnimalLblComentarios.setText("" + animalSeleccionado.getComentarios());
            AnimalLblFoto.setIcon(animalSeleccionado.getFoto());
            AnimalBtnGuardar.setVisible(false);
            AnimalBtnEditar.setVisible(true);
            AnimalBtnAgregar.setVisible(true);
            AnimalChkBxAdoptable.setSelected(animalSeleccionado.isAdoptable());
            if(animalSeleccionado.getTutor()!= null){
                AnimalLblTutor.setText("El tutor del animal es: "+animalSeleccionado.getTutor().getNombre()+".");
            }
            List<Usuario> listaUsuarios = sistema.getUsuarios();
            String padrinosDelAnimal = "";
            for(Usuario usuario : listaUsuarios){
                for(Animal animalApadrinado : usuario.getAnimalesApadrinados()){
                    if(animalSeleccionado.getId() == animalApadrinado.getId()){
                        if(padrinosDelAnimal.trim().equals("") || padrinosDelAnimal.isEmpty()){
                            padrinosDelAnimal = padrinosDelAnimal+usuario.getNombre();
                        }else{
                            padrinosDelAnimal = padrinosDelAnimal+", "+usuario.getNombre();
                        }
                    }
                }
            }
            if(!(padrinosDelAnimal.trim().equals("") || padrinosDelAnimal.isEmpty())){
                if(padrinosDelAnimal.split(",").length > 1){
                    padrinosDelAnimal = "Los padrinos del animal son: "+padrinosDelAnimal+".";
                }else{
                    padrinosDelAnimal = "El padrino del animal es: "+padrinosDelAnimal+".";
                }
            }
            System.out.println(padrinosDelAnimal);
            AnimalLblPadrino.setText(padrinosDelAnimal);
        } else {
            AnimalLblNombre.setText("Nombre:");
            AnimalLblPeso.setText("Peso:");
            AnimalLblAltura.setText("Altura:");
            AnimalLblComentarios.setText("Comentarios:");
            AnimalLblFoto.setIcon(crearIcono("/images/avatar_default.png", 100));
            AnimalBtnEditar.setVisible(false);
        }
    }
    
    public void resetearPestanaAnimales() {
        setearListaAnimales();
        cargarValoresGenericosVentana();
        ocultarAgregarAnimal();
        rutaImagenAgregar = "";
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AnimalBtnAgregar;
    private javax.swing.JButton AnimalBtnEditar;
    private javax.swing.JButton AnimalBtnEliminar;
    private javax.swing.JButton AnimalBtnExaminar;
    private javax.swing.JButton AnimalBtnGuardar;
    private javax.swing.JCheckBox AnimalChkBxAdoptable;
    private javax.swing.JComboBox<String> AnimalComboAnimales;
    private javax.swing.JLabel AnimalLblAdvertencia;
    private javax.swing.JLabel AnimalLblAltura;
    private javax.swing.JLabel AnimalLblAnimales;
    private javax.swing.JLabel AnimalLblComentarios;
    private javax.swing.JLabel AnimalLblFoto;
    private javax.swing.JLabel AnimalLblFotoBoton;
    private javax.swing.JLabel AnimalLblNombre;
    private javax.swing.JLabel AnimalLblPadrino;
    private javax.swing.JLabel AnimalLblPeso;
    private javax.swing.JLabel AnimalLblTutor;
    private javax.swing.JPanel AnimalPanFoto;
    private javax.swing.JPanel AnimalPanInformacion;
    private javax.swing.JSpinner AnimalSpinAltura;
    private javax.swing.JSpinner AnimalSpinPeso;
    private javax.swing.JTextField AnimalTxtComentarios;
    private javax.swing.JTextField AnimalTxtNombre;
    private javax.swing.JLabel CalLblCentimetros;
    private javax.swing.JLabel CalLblKilogramos;
    private javax.swing.JPanel panAnimales;
    // End of variables declaration//GEN-END:variables
}
