package interfaz;

import dominio.*;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 * @author gabomir
 * @author natagarc
 */
public class Calendario extends javax.swing.JPanel{
    private Sistema sistema;
    private boolean agregarAnimalSeleccionado;
    private boolean agregarUsuarioSeleccionado;
    private String rutaImagenAgregar;
    private Fecha fechaSeleccionada;
    private final int[] arrayDiasEnMes;
    private String rutaImagenRuta;

    public Calendario(Sistema sis) {
        sistema = sis;
        fechaSeleccionada = new Fecha();
        initComponents();
        CalPanHoraPersonalizada.setVisible(false);
        CalPanVeterinaria.setVisible(false);
        CalComboVeterinaria.setVisible(false);
        CalBtnVerRuta.setVisible(false);
        CalBtnVeterinariaNo.setSelected(true);
        CalBtnRealizadaNo.setSelected(true);
        CalTxtTipoAlimento.setVisible(false);
        CalLblTipoAlimento.setVisible(false);
        CalPanVeterinaria.setVisible(false);
        CalBtnEditar.setVisible(false);
        CalLblHorarios.setVisible(false);
        rutaImagenAgregar = "";
        rutaImagenRuta = "";
        agregarAnimalSeleccionado = true;
        agregarUsuarioSeleccionado = true;
        arrayDiasEnMes = new int[]{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        CalPanRuta.setVisible(false);
        this.setSize(950, 625);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        panCalendario = new javax.swing.JPanel();
        CalDayChooser = new com.toedter.calendar.JDayChooser();
        CalMonthChooser = new com.toedter.calendar.JMonthChooser();
        CalYearChooser = new com.toedter.calendar.JYearChooser();
        CalScrollActividades = new javax.swing.JScrollPane();
        CalLstActividades = new javax.swing.JList<>();
        CalLblTituloFecha = new javax.swing.JLabel();
        CalLblActividadesDelDia = new javax.swing.JLabel();
        CalScrollInfoAct = new javax.swing.JScrollPane();
        CalTxtAreaInfoAct = new javax.swing.JTextArea();
        CalLblInfoActividad = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        CalLblUsuario = new javax.swing.JLabel();
        CalLblTituloActividad = new javax.swing.JLabel();
        CalLblNombre = new javax.swing.JLabel();
        CalLblFechaResp = new javax.swing.JLabel();
        CalLblAnimal = new javax.swing.JLabel();
        CalLblHora = new javax.swing.JLabel();
        CalLblTipo = new javax.swing.JLabel();
        CalComboTipo = new javax.swing.JComboBox<>();
        CalComboAnimal = new javax.swing.JComboBox<>();
        CalComboHora = new javax.swing.JComboBox<>();
        CalTxtNombreResp = new javax.swing.JTextField();
        CalComboUsuario = new javax.swing.JComboBox<>();
        CalLblFecha = new javax.swing.JLabel();
        CalBtnRealizadaSi = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        CalPanVeterinaria = new javax.swing.JPanel();
        CalLblVeterinaria = new javax.swing.JLabel();
        CalBtnVeterinariaSi = new javax.swing.JRadioButton();
        CalBtnVeterinariaNo = new javax.swing.JRadioButton();
        CalComboVeterinaria = new javax.swing.JComboBox<>();
        CalLblMotivo = new javax.swing.JLabel();
        CalLblHorarios = new javax.swing.JLabel();
        CalComboMotivo = new javax.swing.JComboBox<>();
        CalPanHoraPersonalizada = new javax.swing.JPanel();
        CalPanLblMinutos = new javax.swing.JLabel();
        CalPanSpinMinutos = new javax.swing.JSpinner();
        CalPanSpinHora = new javax.swing.JSpinner();
        CalPanLblHora = new javax.swing.JLabel();
        CalBtnRealizadaNo = new javax.swing.JRadioButton();
        CalLblRealizada = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        CalTxtTipoAlimento = new javax.swing.JTextField();
        CalLblTipoAlimento = new javax.swing.JLabel();
        CalPanRuta = new javax.swing.JPanel();
        CalSpinDistancia = new javax.swing.JSpinner();
        CalLblKilometros = new javax.swing.JLabel();
        CalBtnRuta = new javax.swing.JButton();
        CalLblRuta = new javax.swing.JLabel();
        CalLblDistancia = new javax.swing.JLabel();
        CalBtnVerRuta = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        CalBtnAgregarDatos = new javax.swing.JButton();
        CalBtnEditar = new javax.swing.JButton();
        CalBtnAgregar = new javax.swing.JButton();

        setMaximumSize(new java.awt.Dimension(32767, 572));
        setMinimumSize(new java.awt.Dimension(0, 572));
        setPreferredSize(new java.awt.Dimension(1069, 572));

        panCalendario.setAlignmentY(0.0F);
        panCalendario.setPreferredSize(new java.awt.Dimension(883, 633));

        CalDayChooser.setBorder(new javax.swing.border.MatteBorder(null));
        CalDayChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                CalDayChooserPropertyChange(evt);
            }
        });

        CalMonthChooser.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalMonthChooser.setToolTipText("");

        CalYearChooser.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalLstActividades.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalLstActividades.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                CalLstActividadesValueChanged(evt);
            }
        });
        CalScrollActividades.setViewportView(CalLstActividades);

        CalLblTituloFecha.setText("Fecha");

        CalLblActividadesDelDia.setText("Actividades del Día:");

        CalTxtAreaInfoAct.setEditable(false);
        CalTxtAreaInfoAct.setColumns(20);
        CalTxtAreaInfoAct.setRows(5);
        CalTxtAreaInfoAct.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 102), 1, true));
        CalScrollInfoAct.setViewportView(CalTxtAreaInfoAct);

        CalLblInfoActividad.setText("Información de la Actividad: ");

        jPanel2.setAlignmentY(0.0F);
        jPanel2.setMinimumSize(new java.awt.Dimension(524, 524));
        jPanel2.setPreferredSize(new java.awt.Dimension(700, 524));

        CalLblUsuario.setText("Usuario:");

        CalLblTituloActividad.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        CalLblTituloActividad.setForeground(new java.awt.Color(0, 102, 102));
        CalLblTituloActividad.setText("Actividad");

        CalLblNombre.setText("Nombre:");

        CalLblAnimal.setText("Animal:");

        CalLblHora.setText("Hora");

        CalLblTipo.setText("Tipo de actividad:");

        CalComboTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Paseo", "Alimentación", "Otra" }));
        CalComboTipo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalComboTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalComboTipoActionPerformed(evt);
            }
        });

        CalComboAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalComboHora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ahora", "Personalizado" }));
        CalComboHora.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalComboHora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalComboHoraActionPerformed(evt);
            }
        });

        CalTxtNombreResp.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalTxtNombreResp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalTxtNombreRespActionPerformed(evt);
            }
        });

        CalComboUsuario.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalLblFecha.setText("Fecha:");

        buttonGroup1.add(CalBtnRealizadaSi);
        CalBtnRealizadaSi.setText("Si");
        CalBtnRealizadaSi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalBtnRealizadaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnRealizadaSiActionPerformed(evt);
            }
        });

        jPanel5.setMinimumSize(new java.awt.Dimension(200, 200));

        CalLblVeterinaria.setText("Veterinaria:");

        CalBtnVeterinariaSi.setText("Si");
        CalBtnVeterinariaSi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalBtnVeterinariaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnVeterinariaSiActionPerformed(evt);
            }
        });

        CalBtnVeterinariaNo.setText("No");
        CalBtnVeterinariaNo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalBtnVeterinariaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnVeterinariaNoActionPerformed(evt);
            }
        });

        CalComboVeterinaria.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalComboVeterinaria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalComboVeterinariaActionPerformed(evt);
            }
        });

        CalLblMotivo.setText("Motivo:");

        CalComboMotivo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Corte de Pelo", "Corte de Uñas", "Visita Médica", "Otro" }));
        CalComboMotivo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        javax.swing.GroupLayout CalPanVeterinariaLayout = new javax.swing.GroupLayout(CalPanVeterinaria);
        CalPanVeterinaria.setLayout(CalPanVeterinariaLayout);
        CalPanVeterinariaLayout.setHorizontalGroup(
            CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalPanVeterinariaLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(CalComboVeterinaria, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CalLblHorarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(CalPanVeterinariaLayout.createSequentialGroup()
                        .addGroup(CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CalLblVeterinaria)
                            .addComponent(CalLblMotivo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CalComboMotivo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(CalPanVeterinariaLayout.createSequentialGroup()
                                .addComponent(CalBtnVeterinariaSi)
                                .addGap(18, 18, 18)
                                .addComponent(CalBtnVeterinariaNo)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        CalPanVeterinariaLayout.setVerticalGroup(
            CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalPanVeterinariaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalLblVeterinaria, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalBtnVeterinariaSi)
                    .addComponent(CalBtnVeterinariaNo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CalComboVeterinaria, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CalPanVeterinariaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalComboMotivo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalLblMotivo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(CalLblHorarios, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        CalPanLblMinutos.setText("Minutos");

        CalPanSpinMinutos.setModel(new javax.swing.SpinnerNumberModel(0, 0, 55, 5));
        CalPanSpinMinutos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalPanSpinHora.setModel(new javax.swing.SpinnerListModel(new String[] {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"}));
        CalPanSpinHora.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalPanLblHora.setText("Hora");

        javax.swing.GroupLayout CalPanHoraPersonalizadaLayout = new javax.swing.GroupLayout(CalPanHoraPersonalizada);
        CalPanHoraPersonalizada.setLayout(CalPanHoraPersonalizadaLayout);
        CalPanHoraPersonalizadaLayout.setHorizontalGroup(
            CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalPanHoraPersonalizadaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CalPanLblHora)
                    .addComponent(CalPanLblMinutos))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(CalPanSpinHora)
                    .addComponent(CalPanSpinMinutos, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE))
                .addContainerGap())
        );
        CalPanHoraPersonalizadaLayout.setVerticalGroup(
            CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalPanHoraPersonalizadaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalPanLblHora, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalPanSpinHora, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CalPanHoraPersonalizadaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CalPanLblMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalPanSpinMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(CalPanVeterinaria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(CalPanHoraPersonalizada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CalPanVeterinaria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(CalPanHoraPersonalizada, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        buttonGroup1.add(CalBtnRealizadaNo);
        CalBtnRealizadaNo.setText("No");
        CalBtnRealizadaNo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalBtnRealizadaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnRealizadaNoActionPerformed(evt);
            }
        });

        CalLblRealizada.setText("Fue realizada:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        CalTxtTipoAlimento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));
        CalTxtTipoAlimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalTxtTipoAlimentoActionPerformed(evt);
            }
        });

        CalLblTipoAlimento.setText("Tipo Alimento:");

        CalSpinDistancia.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, null, 0.5d));
        CalSpinDistancia.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 102, 102)));

        CalLblKilometros.setText("Kilómetros");

        CalBtnRuta.setText("Agregar Ruta");
        CalBtnRuta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnRutaActionPerformed(evt);
            }
        });

        CalLblRuta.setText("Ruta:");

        CalLblDistancia.setText("Distancia:");

        CalBtnVerRuta.setText("Ver Ruta");
        CalBtnVerRuta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnVerRutaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CalPanRutaLayout = new javax.swing.GroupLayout(CalPanRuta);
        CalPanRuta.setLayout(CalPanRutaLayout);
        CalPanRutaLayout.setHorizontalGroup(
            CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CalPanRutaLayout.createSequentialGroup()
                .addGroup(CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(CalLblRuta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CalLblDistancia, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                .addGap(40, 40, 40)
                .addGroup(CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CalPanRutaLayout.createSequentialGroup()
                        .addComponent(CalSpinDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CalLblKilometros))
                    .addGroup(CalPanRutaLayout.createSequentialGroup()
                        .addComponent(CalBtnRuta)
                        .addGap(37, 37, 37)
                        .addComponent(CalBtnVerRuta)))
                .addContainerGap(83, Short.MAX_VALUE))
        );
        CalPanRutaLayout.setVerticalGroup(
            CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CalPanRutaLayout.createSequentialGroup()
                .addGroup(CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalBtnRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalLblRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalBtnVerRuta, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(CalPanRutaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalLblDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalSpinDistancia, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalLblKilometros)))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(CalLblTipoAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(CalTxtTipoAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalPanRuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 81, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalTxtTipoAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalLblTipoAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(CalPanRuta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(138, 138, 138)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(CalLblRealizada))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalLblHora))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalLblFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalLblTipo))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalLblTituloActividad))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(CalLblNombre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                            .addComponent(CalLblUsuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CalLblAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(14, 14, 14)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(CalBtnRealizadaSi)
                        .addGap(18, 18, 18)
                        .addComponent(CalBtnRealizadaNo))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(CalTxtNombreResp, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalComboAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalComboUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalComboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalComboHora, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblFechaResp, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CalLblTituloActividad)
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalComboTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalTxtNombreResp, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalComboUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalLblAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalComboAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(CalLblFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblFechaResp, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalComboHora, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CalLblHora, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CalBtnRealizadaSi)
                            .addComponent(CalBtnRealizadaNo)
                            .addComponent(CalLblRealizada, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(123, 123, 123)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51))
        );

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout panCalendarioLayout = new javax.swing.GroupLayout(panCalendario);
        panCalendario.setLayout(panCalendarioLayout);
        panCalendarioLayout.setHorizontalGroup(
            panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panCalendarioLayout.createSequentialGroup()
                .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panCalendarioLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panCalendarioLayout.createSequentialGroup()
                                .addComponent(CalScrollActividades, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(CalScrollInfoAct, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panCalendarioLayout.createSequentialGroup()
                                .addComponent(CalLblTituloFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(CalMonthChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(CalYearChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(CalDayChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panCalendarioLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(CalLblActividadesDelDia, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(CalLblInfoActividad, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(42, 42, 42)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 68, Short.MAX_VALUE))
        );
        panCalendarioLayout.setVerticalGroup(
            panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panCalendarioLayout.createSequentialGroup()
                .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panCalendarioLayout.createSequentialGroup()
                        .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panCalendarioLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(CalYearChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(CalLblTituloFecha, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(CalMonthChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(CalDayChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25)
                                .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(CalLblActividadesDelDia)
                                    .addComponent(CalLblInfoActividad))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(CalScrollInfoAct)
                                    .addComponent(CalScrollActividades, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 7, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 427, Short.MAX_VALUE))
                .addGap(82, 82, 82))
        );

        CalBtnAgregarDatos.setText("Agregar Datos Precargados");
        CalBtnAgregarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnAgregarDatosActionPerformed(evt);
            }
        });

        CalBtnEditar.setText("Editar");
        CalBtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnEditarActionPerformed(evt);
            }
        });

        CalBtnAgregar.setText("Guardar");
        CalBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CalBtnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CalBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CalBtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(CalBtnAgregarDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(398, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CalBtnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalBtnAgregarDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CalBtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(panCalendario, javax.swing.GroupLayout.DEFAULT_SIZE, 1094, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panCalendario, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void CalBtnRealizadaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnRealizadaNoActionPerformed
        CalPanRuta.setVisible(false);
    }//GEN-LAST:event_CalBtnRealizadaNoActionPerformed

    private void CalBtnRealizadaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnRealizadaSiActionPerformed
        if (CalComboTipo.getSelectedIndex() == 0) {
            CalPanRuta.setVisible(true);
        } else {
            CalPanRuta.setVisible(false);
        }
    }//GEN-LAST:event_CalBtnRealizadaSiActionPerformed

    private void CalTxtNombreRespActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalTxtNombreRespActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CalTxtNombreRespActionPerformed

    private void CalComboVeterinariaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalComboVeterinariaActionPerformed
        Veterinaria vet = sistema.buscarVetPorNombre((String) CalComboVeterinaria.getSelectedItem());
        CalLblHorarios.setVisible(true);
        CalLblHorarios.setText("Horarios: " + vet.getHoraInicial() + " - " + vet.getHoraFinal());
    }//GEN-LAST:event_CalComboVeterinariaActionPerformed

    private void CalBtnVeterinariaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnVeterinariaNoActionPerformed
        CalComboVeterinaria.setVisible(false);
        CalComboMotivo.setVisible(false);
        CalLblMotivo.setVisible(false);
        CalLblHorarios.setVisible(false);
    }//GEN-LAST:event_CalBtnVeterinariaNoActionPerformed

    private void CalBtnVeterinariaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnVeterinariaSiActionPerformed
        CalComboVeterinaria.setVisible(true);
        CalComboMotivo.setVisible(true);
        CalLblMotivo.setVisible(true);
        CalLblHorarios.setVisible(true);
    }//GEN-LAST:event_CalBtnVeterinariaSiActionPerformed

    private void CalComboHoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalComboHoraActionPerformed
        if (CalComboHora.getSelectedIndex() == 0) {
            CalPanHoraPersonalizada.setVisible(false);
        } else {
            CalPanHoraPersonalizada.setVisible(true);
        }
    }//GEN-LAST:event_CalComboHoraActionPerformed

    private void CalBtnRutaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnRutaActionPerformed
        JFileChooser elegirImagen = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        elegirImagen.setFileFilter(filter);
        int result = elegirImagen.showDialog(this, "Elija una imagen");
        if (result == JFileChooser.APPROVE_OPTION) {
            File imgSeleccionada = elegirImagen.getSelectedFile();
            rutaImagenRuta = imgSeleccionada.getAbsolutePath();
        } else {
            JOptionPane.showMessageDialog(this, "La ruta no es valida, o no pudo agregarse.", "No se pudo ingresar la ruta", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_CalBtnRutaActionPerformed

    private void CalComboTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalComboTipoActionPerformed
        if (CalComboTipo.getSelectedIndex() == 2) {
            if (CalBtnVeterinariaSi.isSelected()) {
                cambiarVisibilidadVeterinaria(true, true);
            } else {
                cambiarVisibilidadVeterinaria(true, false);
            }
        } else {
            cambiarVisibilidadVeterinaria(false, false);
        }
        if (CalComboTipo.getSelectedIndex() == 1) {
            CalLblTipoAlimento.setVisible(true);
            CalTxtTipoAlimento.setVisible(true);
        } else {
            CalLblTipoAlimento.setVisible(false);
            CalTxtTipoAlimento.setVisible(false);
            CalTxtTipoAlimento.setText("");
        }
        if (CalComboTipo.getSelectedIndex() == 0 && CalBtnRealizadaSi.isSelected()) {
            CalPanRuta.setVisible(true);
        } else {
            CalPanRuta.setVisible(false);
        }
    }//GEN-LAST:event_CalComboTipoActionPerformed

    private void CalTxtTipoAlimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalTxtTipoAlimentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CalTxtTipoAlimentoActionPerformed

    private void CalBtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnEditarActionPerformed
        CalLstActividades.setEnabled(false);
        String nombreAct = CalLstActividades.getSelectedValue();
        Paseo paseo = sistema.buscarPaseoPorNombre(nombreAct);
        CalBtnVeterinariaNo.setSelected(true);
        CalBtnVeterinariaNoActionPerformed(evt);
        //CalBtnGroupRealizada.clearSelection();
        CalBtnRealizadaNo.setSelected(true);
        if (paseo != null) {
            if (paseo.getFueRealizado()) {
                CalBtnRealizadaSi.setSelected(true);
            }
            CalPanRuta.setVisible(true);
            CalComboTipo.setSelectedIndex(0);
            if (paseo.getFueRealizado()) {
                CalPanRuta.setVisible(true);
            }
            if (paseo.getRuta() == null) {
                CalBtnVerRuta.setVisible(false);
            }
            CalTxtNombreResp.setText(nombreAct);
            CalComboUsuario.setSelectedItem(paseo.getUsuario().getNombre());
            CalComboAnimal.setSelectedItem(paseo.getMascota().getNombre());
            CalLblFechaResp.setText(paseo.getFecha().getDia() + "/" + paseo.getFecha().getMes() + "/" + paseo.getFecha().getAno());
            CalComboHora.setSelectedIndex(1);
            int hora = paseo.getHora().getHour();
            String horaString = "" + hora;
            if (hora < 10) {
                horaString = "0" + horaString;
            }
            CalPanSpinHora.setValue((Object) horaString);
            CalPanSpinMinutos.setValue(paseo.getHora().getMinute());
            sistema.EliminarActividad(paseo);
            sistema.getPaseos().remove(paseo);
        } else {
            Alimentacion alim = sistema.buscarAlimentacionPorNombre(nombreAct);
            if (alim != null) {
                if (alim.getFueRealizado()) {
                    CalBtnRealizadaSi.setSelected(true);
                }
                CalComboTipo.setSelectedIndex(1);
                CalTxtNombreResp.setText(nombreAct);
                CalComboUsuario.setSelectedItem(alim.getUsuario().getNombre());
                CalComboAnimal.setSelectedItem(alim.getMascota().getNombre());
                CalLblFechaResp.setText(alim.getFecha().getDia() + "/" + alim.getFecha().getMes() + "/" + alim.getFecha().getAno());
                CalComboHora.setSelectedIndex(1);
                int hora = alim.getHora().getHour();
                String horaString = "" + hora;
                if (hora < 10) {
                    horaString = "0" + horaString;
                }
                CalPanSpinHora.setValue((Object) horaString);
                CalPanSpinMinutos.setValue(alim.getHora().getMinute());
                CalTxtTipoAlimento.setText(alim.getTipoAlimento());
                CalTxtTipoAlimento.setVisible(true);
                sistema.EliminarActividad(alim);
                sistema.getAlimentaciones().remove(alim);
            } else {
                VisitaVeterinaria visita = sistema.buscarVisitaPorNombre(nombreAct);
                if (visita != null) {
                    if (visita.getFueRealizado()) {
                        CalBtnRealizadaSi.setSelected(true);
                    }
                    CalComboTipo.setSelectedIndex(2);
                    CalTxtNombreResp.setText(nombreAct);
                    CalComboUsuario.setSelectedItem(visita.getUsuario().getNombre());
                    CalComboAnimal.setSelectedItem(visita.getMascota().getNombre());
                    CalLblFechaResp.setText(visita.getFecha().getDia() + "/" + visita.getFecha().getMes() + "/" + visita.getFecha().getAno());
                    CalComboHora.setSelectedIndex(1);
                    int hora = visita.getHora().getHour();
                    String horaString = "" + hora;
                    if (hora < 10) {
                        horaString = "0" + horaString;
                    }
                    CalPanSpinHora.setValue((Object) horaString);
                    CalPanSpinMinutos.setValue(visita.getHora().getMinute());
                    CalBtnVeterinariaSi.setSelected(true);
                    CalComboVeterinaria.setSelectedItem(visita.getVeterinaria().getNombre());
                    CalComboMotivo.setSelectedItem(visita.getMotivo());
                    CalPanVeterinaria.setVisible(true);
                    CalLblHorarios.setVisible(true);
                    visita.getVeterinaria().getActividadesAgendadas().remove(visita);
                    sistema.EliminarActividad(visita);
                    sistema.getVisitas().remove(visita);
                } else {
                    ActividadCualquiera actividad = sistema.buscarActCualquieraPorNombre(nombreAct);
                    if (actividad.getFueRealizado()) {
                        CalBtnRealizadaSi.setSelected(true);
                    }
                    CalComboTipo.setSelectedIndex(2);
                    CalTxtNombreResp.setText(nombreAct);
                    CalComboUsuario.setSelectedItem(actividad.getUsuario().getNombre());
                    CalComboAnimal.setSelectedItem(actividad.getMascota().getNombre());
                    CalLblFechaResp.setText(actividad.getFecha().getDia() + "/" + actividad.getFecha().getMes() + "/" + actividad.getFecha().getAno());
                    CalComboHora.setSelectedIndex(1);
                    int hora = actividad.getHora().getHour();
                    String horaString = "" + hora;
                    if (hora < 10) {
                        horaString = "0" + horaString;
                    }
                    CalPanSpinHora.setValue((Object) horaString);
                    CalPanSpinMinutos.setValue(actividad.getHora().getMinute());
                    CalTxtTipoAlimento.setVisible(true);
                    sistema.EliminarActividad(actividad);
                    sistema.getActsCualquieras().remove(actividad);
                }
            }
        }
        CalBtnEditar.setVisible(false);
    }//GEN-LAST:event_CalBtnEditarActionPerformed

    private void CalBtnVerRutaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnVerRutaActionPerformed
        String nombreAct = CalLstActividades.getSelectedValue();
        Paseo paseo = sistema.buscarPaseoPorNombre(nombreAct);
        if (paseo != null) {
            if (paseo.getRuta() != null) {
                try {
                    new VentanaFotoRuta(paseo).setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(this, "El paseo seleccionado no tiene una ruta agregada.", "Paseo Sin Ruta", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_CalBtnVerRutaActionPerformed

    private void CalBtnAgregarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnAgregarDatosActionPerformed
        try {
            Usuario persona = new Usuario("Alex", "alexkmass@gmail.com");
            sistema.AnadirUsuario(persona);
            Animal rasta = new Animal("Rasta", 50, 23, "Es un buen perro, le gusta comer", true);
            rasta.setFoto(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_2.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
            sistema.AnadirAnimal(rasta);
            Usuario persona2 = new Usuario("Marcelo", "marcelo@gmail.com");
            sistema.AnadirUsuario(persona2);
            Animal ori = new Animal("Ori", 50, 23, "Es un buen perro", true);
            ori.setFoto(new ImageIcon(ImageIO.read(this.getClass().getResource("images/avatar_1.png")).getScaledInstance(100, 100, java.awt.Image.SCALE_SMOOTH)));
            sistema.AnadirAnimal(ori);
            Fecha fecha = new Fecha(25, 11, 2019);
            sistema.AnadirFecha(fecha);
            LocalTime hora = LocalTime.now();
            Paseo act = new Paseo("Paseo Rasta", persona, rasta, 0.0, false, hora, fecha);
            act.setRuta(new ImageIcon(ImageIO.read(this.getClass().getResource("images/rutaPorDefecto.png")).getScaledInstance(500, 500, java.awt.Image.SCALE_SMOOTH)));
            sistema.AnadirActividad(act);
            sistema.getPaseos().add(act);
            hora = LocalTime.now();
            Alimentacion act2 = new Alimentacion("Alimentación de Ori", persona, ori, "DogChow", false, hora, fecha);
            sistema.AnadirActividad(act2);
            sistema.getAlimentaciones().add(act2);
            ActividadCualquiera act3 = new ActividadCualquiera("Ir a una cita con Rasta", persona, rasta, true, hora, fecha);
            sistema.AnadirActividad(act3);
            sistema.getActsCualquieras().add(act3);
            Veterinaria vet1 = new Veterinaria("Pocitos", new ArrayList<>(), 8, 16);
            sistema.getVeterinarias().add(vet1);
            Veterinaria vet2 = new Veterinaria("Carrasco", new ArrayList<>(), 10, 15);
            sistema.getVeterinarias().add(vet2);
            Veterinaria vet3 = new Veterinaria("Cordon", new ArrayList<>(), 8, 23);
            sistema.getVeterinarias().add(vet3);
            VisitaVeterinaria visita = new VisitaVeterinaria("Visita a VetCordon", hora, persona, rasta, false, fecha, vet3, "Rasta tiene fiebre");
            sistema.AnadirActividad(visita);
            sistema.getVisitas().add(visita);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        resetearListaAnimales();
        resetearListaUsuarios();
        setearListaDeVeterinarias();
        CalBtnAgregarDatos.setVisible(false);
        int dia = CalDayChooser.getDay();
        int mes = CalMonthChooser.getMonth() + 1;
        int ano = CalYearChooser.getYear();
        ArrayList<Actividad> listaActividades = sistema.listaActividadesPorFecha(dia, mes, ano);
        String[] arrActividades = new String[listaActividades.size()];
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividad act = listaActividades.get(i);
            arrActividades[i] = act.getNombre();
        }
        CalLstActividades.setListData(arrActividades);

        CalLblFechaResp.setText(dia + "/" + mes + "/" + ano);
        fechaSeleccionada = new Fecha(dia, mes, ano);
    }//GEN-LAST:event_CalBtnAgregarDatosActionPerformed

    private void CalBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CalBtnAgregarActionPerformed
        String nombreAct = CalTxtNombreResp.getText();
        boolean fueRealizada = true;
        if (CalBtnRealizadaNo.isSelected()) {
            fueRealizada = false;
        }
        if (nombreAct.equals("")) {
            JOptionPane.showMessageDialog(this, "Debe ingresar un nombre para poder realizar la actividad.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
        } else if (CalComboUsuario.getItemCount() == 0) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un usuario para poder realizar la actividad.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
        } else if (CalComboAnimal.getItemCount() == 0) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un animal para poder realizar la actividad.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
        } else {
            Integer idUsuario = Integer.parseInt(CalComboUsuario.getSelectedItem().toString().split(" - ")[0]);
            Usuario usuario = sistema.buscarUsuarioPorId(idUsuario);
            String idAnimal = CalComboAnimal.getSelectedItem().toString().split(" - ")[0];
            Animal animal = sistema.buscarAnimalPorNombre(idAnimal);
            LocalTime time;
            if (((String) CalComboHora.getSelectedItem()).equals("Ahora")) {
                time = LocalTime.now();
            } else {
                String hora = CalPanSpinHora.getValue() + "";
                String min = CalPanSpinMinutos.getValue() + "";
                time = LocalTime.of(Integer.parseInt(hora), Integer.parseInt(min));
            }
            if (CalBtnVeterinariaSi.isSelected()) {
                Veterinaria vet = sistema.buscarVetPorNombre((String) CalComboVeterinaria.getSelectedItem());
                String motivo = (String) CalComboMotivo.getSelectedItem();
                VisitaVeterinaria visita = new VisitaVeterinaria(nombreAct, time, usuario, animal, fueRealizada, fechaSeleccionada, vet, motivo);
                if (vet.AgendarActividad(visita)) {
                    sistema.AnadirActividad(visita);
                    sistema.getVisitas().add(visita);
                    if (CalComboHora.getSelectedIndex() != 0) {
                        timerNuevo(visita);
                    }
                    JOptionPane.showMessageDialog(this, "Se registro exitosamente la visita a la veterinaria.", "Registro Exitoso", JOptionPane.INFORMATION_MESSAGE);
                    CalLstActividades.setEnabled(true);
                    CalBtnEditar.setVisible(false);
                    CalTxtNombreResp.setText("");
                    CalPanVeterinaria.setVisible(false);
                    CalLblHorarios.setVisible(false);
                } else {
                    JOptionPane.showMessageDialog(this, "Debe ingresar una hora entre las "+vet.getHoraInicial()+" y las "+vet.getHoraFinal()+".", "Horario no disponible en veterinaria", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                String tipoAct = (String) CalComboTipo.getSelectedItem();
                if (tipoAct.equals("Paseo")) {
                    Paseo paseo = new Paseo(nombreAct, usuario, animal, 0, fueRealizada, time, fechaSeleccionada);
                    double distanciaPaseo = Double.parseDouble(CalSpinDistancia.getValue().toString());
                    if (distanciaPaseo != 0.0) {
                        paseo.setDistancia(distanciaPaseo);
                    }
                    sistema.AnadirActividad(paseo);
                    sistema.getPaseos().add(paseo);
                    if (CalComboHora.getSelectedIndex() != 0) {
                        timerNuevo(paseo);
                    }
                    if (!rutaImagenRuta.equals("")) {
                        File imagen = new File(rutaImagenRuta);
                        paseo.setRuta(crearIcono(imagen, 500));
                        rutaImagenRuta = "";
                    }
                    JOptionPane.showMessageDialog(this, "Se añadio el paseo correctamente al calendario.", "Registro Exitoso", JOptionPane.INFORMATION_MESSAGE);
                    CalLstActividades.setEnabled(true);
                    CalTxtNombreResp.setText("");
                    CalBtnEditar.setVisible(false);
                    CalPanRuta.setVisible(false);
                } else {
                    if (tipoAct.equals("Alimentación")) {
                        String tipoAlimento = CalTxtTipoAlimento.getText();
                        if (tipoAlimento.equals("")) {
                            JOptionPane.showMessageDialog(this, "Debe ingresar el tipo de alimento para poder registrar la alimentación.", "Campo obligatorio incompleto", JOptionPane.WARNING_MESSAGE);
                        } else {
                            Alimentacion alim = new Alimentacion(nombreAct, usuario, animal, tipoAlimento, fueRealizada, time, fechaSeleccionada);
                            sistema.AnadirActividad(alim);
                            sistema.getAlimentaciones().add(alim);
                            if (CalComboHora.getSelectedIndex() != 0) {
                                timerNuevo(alim);
                            }
                            JOptionPane.showMessageDialog(this, "Se añadió la alimentación correctamente al calendario.", "Registro Exitoso", JOptionPane.INFORMATION_MESSAGE);
                            CalTxtNombreResp.setText("");
                            CalTxtTipoAlimento.setText("");
                            CalBtnEditar.setVisible(false);
                            CalLstActividades.setEnabled(true);
                        }
                    } else {
                        ActividadCualquiera actividad = new ActividadCualquiera(nombreAct, usuario, animal, fueRealizada, time, fechaSeleccionada);
                        sistema.AnadirActividad(actividad);
                        sistema.getActsCualquieras().add(actividad);
                        if (CalComboHora.getSelectedIndex() != 0) {
                            timerNuevo(actividad);
                        }
                        JOptionPane.showMessageDialog(this, "Se añadió la actividad correctamente al calendario.", "Registro Exitoso", JOptionPane.INFORMATION_MESSAGE);
                        CalTxtNombreResp.setText("");
                        CalBtnEditar.setVisible(false);
                        CalLstActividades.setEnabled(true);
                    }
                }
            }
        }
        CalPanRuta.setVisible(false);
        int dia = CalDayChooser.getDay();
        int mes = CalMonthChooser.getMonth() + 1;
        int ano = CalYearChooser.getYear();
        ArrayList<Actividad> listaActividades = sistema.listaActividadesPorFecha(dia, mes, ano);
        String[] arrActividades = new String[listaActividades.size()];
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividad act = listaActividades.get(i);
            arrActividades[i] = act.getNombre();
        }
        CalLstActividades.setListData(arrActividades);

        CalLblFechaResp.setText(dia + "/" + mes + "/" + ano);
        fechaSeleccionada = new Fecha(dia, mes, ano);
    }//GEN-LAST:event_CalBtnAgregarActionPerformed

    private void CalLstActividadesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_CalLstActividadesValueChanged
        String nombreAct = CalLstActividades.getSelectedValue();
        String cero = "";
        CalBtnVerRuta.setVisible(false);
        if (nombreAct != null) {
            Paseo paseo = sistema.buscarPaseoPorNombre(nombreAct);
            if (paseo != null) {
                CalBtnVerRuta.setVisible(true);
                if (paseo.getHora().getMinute() < 10) {
                    cero = "0";
                }
                String fueRealizada = "No";
                if (paseo.getFueRealizado()) {
                    fueRealizada = "Si";
                }
                if (paseo.getDistancia() == 0.0) {
                    CalTxtAreaInfoAct.setText("Nombre: " + paseo.getNombre() + "\n"
                        + "Usuario responsable: " + paseo.getUsuario().getNombre() + "\n"
                        + "Animal: " + paseo.getMascota().getNombre() + "\n"
                        + "Realizada: " + fueRealizada + "\n"
                        + "Hora: " + paseo.getHora().getHour() + ":" + cero + paseo.getHora().getMinute());
                } else {
                    CalTxtAreaInfoAct.setText("Nombre: " + paseo.getNombre() + "\n"
                        + "Usuario responsable: " + paseo.getUsuario().getNombre() + "\n"
                        + "Animal: " + paseo.getMascota().getNombre() + "\n"
                        + "Realizada: " + fueRealizada + "\n"
                        + "Hora: " + paseo.getHora().getHour() + ":" + cero + paseo.getHora().getMinute() + "\n"
                        + "Distancia: " + paseo.getDistancia() + "kilómetros");
                }
                if (paseo.getRuta() != null) {

                }
            } else {
                Alimentacion alim = sistema.buscarAlimentacionPorNombre(nombreAct);
                if (alim != null) {
                    if (alim.getHora().getMinute() < 10) {
                        cero = "0";
                    }
                    String fueRealizada = "No";
                    if (alim.getFueRealizado()) {
                        fueRealizada = "Si";
                    }

                    CalTxtAreaInfoAct.setText("Nombre: " + alim.getNombre() + "\n"
                        + "Usuario responsable: " + alim.getUsuario().getNombre() + "\n"
                        + "Animal: " + alim.getMascota().getNombre() + "\n"
                        + "Realizada: " + fueRealizada + "\n"
                        + "Tipo Alimento: " + alim.getTipoAlimento() + "\n"
                        + "Hora: " + alim.getHora().getHour() + ":" + cero + alim.getHora().getMinute());
                } else {
                    VisitaVeterinaria visita = sistema.buscarVisitaPorNombre(nombreAct);
                    if (visita != null) {
                        if (visita.getHora().getMinute() < 10) {
                            cero = "0";
                        }
                        String fueRealizada = "No";
                        if (visita.getFueRealizado()) {
                            fueRealizada = "Si";
                        }
                        CalTxtAreaInfoAct.setText("Nombre: " + visita.getNombre() + "\n"
                            + "Usuario responsable: " + visita.getUsuario().getNombre() + "\n"
                            + "Animal: " + visita.getMascota().getNombre() + "\n"
                            + "Realizada: " + fueRealizada + "\n"
                            + "Motivo: " + visita.getMotivo() + "\n"
                            + "Veterinaria: " + visita.getVeterinaria().getNombre() + "\n"
                            + "Hora: " + visita.getHora().getHour() + ":" + cero + visita.getHora().getMinute());
                    } else {
                        ActividadCualquiera act = sistema.buscarActCualquieraPorNombre(nombreAct);
                        if (act.getHora().getMinute() < 10) {
                            cero = "0";
                        }
                        String fueRealizada = "No";
                        if (act.getFueRealizado()) {
                            fueRealizada = "Si";
                        }

                        CalTxtAreaInfoAct.setText("Nombre: " + act.getNombre() + "\n"
                            + "Usuario responsable: " + act.getUsuario().getNombre() + "\n"
                            + "Animal: " + act.getMascota().getNombre() + "\n"
                            + "Realizada: " + fueRealizada + "\n"
                            + "Hora: " + act.getHora().getHour() + ":" + cero + act.getHora().getMinute());
                    }
                }
            }
            CalBtnEditar.setVisible(true);
        } else {
            CalTxtAreaInfoAct.setText("Seleccione un paseo para" + "\n"
                + "poder ver su información");
        }
    }//GEN-LAST:event_CalLstActividadesValueChanged

    private void CalDayChooserPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_CalDayChooserPropertyChange
        int dia = CalDayChooser.getDay();
        int mes = CalMonthChooser.getMonth() + 1;
        int ano = CalYearChooser.getYear();
        ArrayList<Actividad> listaActividades = sistema.listaActividadesPorFecha(dia, mes, ano);
        String[] arrActividades = new String[listaActividades.size()];
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividad act = listaActividades.get(i);
            arrActividades[i] = act.getNombre();
        }
        CalLstActividades.setListData(arrActividades);

        CalLblFechaResp.setText(dia + "/" + mes + "/" + ano);
        fechaSeleccionada = new Fecha(dia, mes, ano);
    }//GEN-LAST:event_CalDayChooserPropertyChange

    private void cambiarVisibilidadVeterinaria(boolean opcionCombo, boolean usaVeterinaria) {
        CalPanVeterinaria.setVisible(opcionCombo);
        CalBtnVeterinariaSi.setSelected(usaVeterinaria);
        CalBtnVeterinariaNo.setSelected(!usaVeterinaria);
        CalComboVeterinaria.setVisible(usaVeterinaria);
        CalComboMotivo.setVisible(usaVeterinaria);
        CalLblMotivo.setVisible(usaVeterinaria);
        CalLblHorarios.setVisible(opcionCombo);
    }
        
    private ImageIcon crearIcono(String ruta, int tamano) {
        ImageIcon retorno = null;
        try {
            retorno = new ImageIcon(ImageIO.read(getClass().getResource(ruta)).getScaledInstance(tamano, -1, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            System.out.println(e);
        }
        return retorno;
    }

    private ImageIcon crearIcono(File file, int tamano) {
        ImageIcon retorno = null;
        try {
            retorno = new ImageIcon(ImageIO.read(file).getScaledInstance(tamano, -1, Image.SCALE_SMOOTH));
        } catch (IOException e) {
            System.out.println(e);
        }
        return retorno;
    }

    private void enviarMail(Actividad actividad) {
        String nombreAct = actividad.getNombre();
        Paseo paseo = sistema.buscarPaseoPorNombre(nombreAct);
        String tipoActividad;
        String cuerpo;
        String cero = "";
        if (paseo != null) {
            tipoActividad = "Paseo";
            if (actividad.getHora().getMinute() < 10) {
                cero = "0";
            }
            cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                    + "Te recordamos que debes pasear a "
                    + actividad.getMascota().getNombre() + " hoy a las "
                    + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                    + "No lo olvides!";
        } else {
            Alimentacion alimentacion = sistema.buscarAlimentacionPorNombre(nombreAct);
            if (alimentacion != null) {
                tipoActividad = "Alimentacion";
                if (actividad.getHora().getMinute() < 10) {
                    cero = "0";
                }
                cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                        + "Te recordamos que debes alimentar a "
                        + actividad.getMascota().getNombre() + " con " + alimentacion.getTipoAlimento()
                        + " hoy a las " + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                        + "No lo olvides!";
            } else {
                VisitaVeterinaria visita = sistema.buscarVisitaPorNombre(nombreAct);
                if (visita != null) {
                    tipoActividad = "Visita a Veterinaria";
                    if (actividad.getHora().getMinute() < 10) {
                        cero = "0";
                    }
                    cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                            + "Te recordamos que debes llevar a "
                            + visita.getMascota().getNombre() + "a la veterinaria " + visita.getVeterinaria().getNombre()
                            + "para realizar un/una " + visita.getMotivo() + " hoy a las "
                            + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                            + "No lo olvides!";
                } else {
                    ActividadCualquiera actividadCualquiera = sistema.buscarActCualquieraPorNombre(nombreAct);
                    tipoActividad = "una Actividad";
                    if (actividad.getHora().getMinute() < 10) {
                        cero = "0";
                    }
                    cuerpo = "Hola " + actividadCualquiera.getUsuario().getNombre() + ",\n"
                            + "Te recordamos que debes " + actividadCualquiera.getNombre() + " con "
                            + actividadCualquiera.getMascota().getNombre() + " hoy a las "
                            + actividadCualquiera.getHora().getHour() + ":" + cero + actividadCualquiera.getHora().getMinute() + ".\n"
                            + "No lo olvides!";
                }
            }
        }
        String asunto = "Recordatorio de " + tipoActividad + " de My Pet";

        final String username = "recordatoriosmypet@gmail.com";
        final String password = "canucanualex";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("recordatoriosmismascotas@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(actividad.getUsuario().getMail()));
            message.setSubject(asunto);
            message.setText(cuerpo);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public void timerNuevo(final Actividad act) {
        int horaActividad = act.getHora().getHour() * 60 + act.getHora().getMinute();
        int horaActual = LocalTime.now().getHour() * 60 + LocalTime.now().getMinute();
        int tiempoRestanteMinutos = horaActividad - horaActual;
        try {
            ActionListener notificacion = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    enviarMail(act);
                    mostrarNotificacion(act);
                }
            };
            int tiempoRestanteMilisegundos = tiempoRestanteMinutos * 60000;
            Timer timer = new Timer(tiempoRestanteMilisegundos, notificacion);
            timer.setRepeats(false);
            timer.start();
        } catch (ArithmeticException e) {

        }
    }

    public void mostrarNotificacion(Actividad actividad) {
        String nombreAct = actividad.getNombre();
        Paseo paseo = sistema.buscarPaseoPorNombre(nombreAct);
        String cuerpo;
        String cero = "";
        if (paseo != null) {
            if (actividad.getHora().getMinute() < 10) {
                cero = "0";
            }
            cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                    + "Te recordamos que debes pasear a "
                    + actividad.getMascota().getNombre() + " hoy a las "
                    + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                    + "No lo olvides!";
        } else {
            Alimentacion alimentacion = sistema.buscarAlimentacionPorNombre(nombreAct);
            if (alimentacion != null) {
                if (actividad.getHora().getMinute() < 10) {
                    cero = "0";
                }
                cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                        + "Te recordamos que debes alimentar a "
                        + actividad.getMascota().getNombre() + " con " + alimentacion.getTipoAlimento()
                        + " hoy a las " + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                        + "No lo olvides!";
            } else {
                VisitaVeterinaria visita = sistema.buscarVisitaPorNombre(nombreAct);
                if (visita != null) {
                    if (actividad.getHora().getMinute() < 10) {
                        cero = "0";
                    }
                    cuerpo = "Hola " + actividad.getUsuario().getNombre() + ",\n"
                            + "Te recordamos que debes llevar a "
                            + visita.getMascota().getNombre() + "a la veterinaria " + visita.getVeterinaria().getNombre()
                            + "para realizar un/una " + visita.getMotivo() + " hoy a las "
                            + actividad.getHora().getHour() + ":" + cero + actividad.getHora().getMinute() + ".\n"
                            + "No lo olvides!";
                } else {
                    if (actividad.getHora().getMinute() < 10) {
                        cero = "0";
                    }
                    ActividadCualquiera actividadCualquiera = sistema.buscarActCualquieraPorNombre(nombreAct);
                    cuerpo = "Hola " + actividadCualquiera.getUsuario().getNombre() + ",\n"
                            + "Te recordamos que debes " + actividadCualquiera.getNombre() + " con "
                            + actividadCualquiera.getMascota().getNombre() + " hoy a las "
                            + actividadCualquiera.getHora().getHour() + ":" + cero + actividadCualquiera.getHora().getMinute() + ".\n"
                            + "No lo olvides!";
                }
            }
        }
        //JFrame ventana = this;
        //JOptionPane.showMessageDialog(ventana, cuerpo, "Notificación", 1);
    }
    
    public void resetearListaUsuarios() {
        if (CalComboUsuario.getItemCount() > 0) {
            CalComboUsuario.removeAllItems();
        }
        ArrayList<Usuario> usuarios = sistema.getUsuarios();
        CalComboUsuario.setModel(new DefaultComboBoxModel(usuarios.toArray()));
    }
    
    public void resetearListaAnimales() {
        if (CalComboAnimal.getItemCount() > 0) {
            CalComboAnimal.removeAllItems();
        }
        for (int i = 0; i < sistema.getAnimales().size(); i++) {
            CalComboAnimal.addItem(sistema.getAnimales().get(i).getNombre());
        }
    }
       
    private void setearListaDeVeterinarias() {
        if (CalComboVeterinaria.getItemCount() > 0) {
            CalComboVeterinaria.removeAllItems();
        }
        for (int i = 0; i < sistema.getVeterinarias().size(); i++) {
            CalComboVeterinaria.addItem(sistema.getVeterinarias().get(i).getNombre());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CalBtnAgregar;
    private javax.swing.JButton CalBtnAgregarDatos;
    private javax.swing.JButton CalBtnEditar;
    private javax.swing.JRadioButton CalBtnRealizadaNo;
    private javax.swing.JRadioButton CalBtnRealizadaSi;
    private javax.swing.JButton CalBtnRuta;
    private javax.swing.JButton CalBtnVerRuta;
    private javax.swing.JRadioButton CalBtnVeterinariaNo;
    private javax.swing.JRadioButton CalBtnVeterinariaSi;
    private javax.swing.JComboBox<String> CalComboAnimal;
    private javax.swing.JComboBox<String> CalComboHora;
    private javax.swing.JComboBox<String> CalComboMotivo;
    private javax.swing.JComboBox<String> CalComboTipo;
    private javax.swing.JComboBox<String> CalComboUsuario;
    private javax.swing.JComboBox<String> CalComboVeterinaria;
    private com.toedter.calendar.JDayChooser CalDayChooser;
    private javax.swing.JLabel CalLblActividadesDelDia;
    private javax.swing.JLabel CalLblAnimal;
    private javax.swing.JLabel CalLblDistancia;
    private javax.swing.JLabel CalLblFecha;
    private javax.swing.JLabel CalLblFechaResp;
    private javax.swing.JLabel CalLblHora;
    private javax.swing.JLabel CalLblHorarios;
    private javax.swing.JLabel CalLblInfoActividad;
    private javax.swing.JLabel CalLblKilometros;
    private javax.swing.JLabel CalLblMotivo;
    private javax.swing.JLabel CalLblNombre;
    private javax.swing.JLabel CalLblRealizada;
    private javax.swing.JLabel CalLblRuta;
    private javax.swing.JLabel CalLblTipo;
    private javax.swing.JLabel CalLblTipoAlimento;
    private javax.swing.JLabel CalLblTituloActividad;
    private javax.swing.JLabel CalLblTituloFecha;
    private javax.swing.JLabel CalLblUsuario;
    private javax.swing.JLabel CalLblVeterinaria;
    private javax.swing.JList<String> CalLstActividades;
    private com.toedter.calendar.JMonthChooser CalMonthChooser;
    private javax.swing.JPanel CalPanHoraPersonalizada;
    private javax.swing.JLabel CalPanLblHora;
    private javax.swing.JLabel CalPanLblMinutos;
    private javax.swing.JPanel CalPanRuta;
    private javax.swing.JSpinner CalPanSpinHora;
    private javax.swing.JSpinner CalPanSpinMinutos;
    private javax.swing.JPanel CalPanVeterinaria;
    private javax.swing.JScrollPane CalScrollActividades;
    private javax.swing.JScrollPane CalScrollInfoAct;
    private javax.swing.JSpinner CalSpinDistancia;
    private javax.swing.JTextArea CalTxtAreaInfoAct;
    private javax.swing.JTextField CalTxtNombreResp;
    private javax.swing.JTextField CalTxtTipoAlimento;
    private com.toedter.calendar.JYearChooser CalYearChooser;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel panCalendario;
    // End of variables declaration//GEN-END:variables

}
