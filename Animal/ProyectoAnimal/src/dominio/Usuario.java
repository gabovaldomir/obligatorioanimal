package dominio;

import java.util.ArrayList;

public class Usuario {
    private Integer id;
    private String nombre;
    private String apellido;
    private String telefono;
    private String mail;
    private String ciudad;
    private String pais;
    private Boolean esPadrino = false;
    private final ArrayList<Actividad> actividades;
    private ArrayList<Animal> animalesApadrinados;
    private String deposito;
    private String moneda;
    private String periodo;
    private String medio;

    public Usuario(String nombre, String apellido, String telefono, String mail, String ciudad, String pais, ArrayList<Animal> animales) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.mail = mail;
        this.ciudad = ciudad;
        this.pais = pais;
        this.actividades = new ArrayList<>();
        this.animalesApadrinados = animales;
    }
    
    public Usuario(String nombre, String mail) {
        this.nombre = nombre;
        this.apellido = "Sin-Apellido";
        this.telefono = "";
        this.mail = mail;
        this.ciudad = "Sin-Ciudad";
        this.pais = "Sin-Pais";
        this.actividades = new ArrayList<>();
        this.animalesApadrinados = new ArrayList<>();
    }
    
    public Usuario() {
        this.nombre = "Sin-Nombre";
        this.apellido = "Sin-Apellido";
        this.telefono = "";
        this.mail = "Sin-Mail";
        this.ciudad = "Sin-Ciudad";
        this.pais = "Sin-Pais";
        this.actividades = new ArrayList<>();
        this.animalesApadrinados = new ArrayList<>();
    }
    
    public Usuario(String nombre, String apellido, String telefono, String mail, String ciudad, String pais, ArrayList<Animal> animales, String deposito, String moneda, String periodo, String medio) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.mail = mail;
        this.ciudad = ciudad;
        this.pais = pais;
        this.actividades = new ArrayList<>();
        this.animalesApadrinados = animales;
        this.deposito = deposito;
        this.moneda = moneda;
        this.periodo = periodo;
        this.medio = medio;
    }

    public ArrayList<Actividad> getActividades() {
        return actividades;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if (nombre.equals("")) {
            this.nombre = "Sin-Nombre";
        } else {
            this.nombre = nombre;
        }
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        if (mail.equals("")) {
            this.mail = "Sin-Mail";
        } else {
            this.mail = mail;
        }
    }
    
    public void agregarActividad(Actividad act){
        actividades.add(act);
    }

    @Override
    public String toString() {
        return id + " - " + nombre + "  " + apellido;
    }

    public ArrayList<Animal>  getAnimalesApadrinados() {
        return animalesApadrinados;
    }
    
    public void setAnimalesApadrinados(ArrayList<Animal> animales) {
        animalesApadrinados = animales;
    }


    public String getApellido() {
        return apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setApellido(String apellido) {
        if (apellido.equals("")) {
            this.apellido = "Sin-Apellido";
        } else {
            this.apellido = apellido;
        }
    }

    public void setTelefono(String telefono) {
        if (telefono.equals("")) {
            this.telefono = "";
        } else {
            this.telefono = telefono;
        }
    }

    public void setCiudad(String ciudad) {
        if (ciudad.equals("")) {
            this.ciudad = "Sin-Ciudad";
        } else {
            this.ciudad = ciudad;
        }
    }

    public void setPais(String pais) {
        if (pais.equals("")) {
            this.pais = "Sin-Pais";
        } else {
            this.pais = pais;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(int nuevoId) {
        id = nuevoId;
    }

    public void setEsPadrino(boolean nuevoEsPadrino) {
        esPadrino = nuevoEsPadrino;
    }

    public Boolean getEspadrino() {
        return esPadrino;
    }

    public String getDeposito() {
        return deposito;
    }
    
    public String getMoneda() {
        return moneda;
    }

    public String getPeriodo() {
        return periodo;
    }

    public String getMedio() {
        return medio;
    }

    public void setDeposito(String nuevoDeposito) {
        deposito = nuevoDeposito;
    }
    
    public void setMoneda(String nuevaMoneda) {
        moneda = nuevaMoneda;
    }
    
    public void setPeriodo(String nuevoPeriodo) {
        periodo = nuevoPeriodo;
    }

    public void setMedio(String nuevoMedio) {
        medio = nuevoMedio;
    }

}
