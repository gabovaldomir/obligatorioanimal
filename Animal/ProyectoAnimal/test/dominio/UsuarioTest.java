/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marce
 */
public class UsuarioTest {

    private Usuario usuario;

    public UsuarioTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        usuario = new Usuario("Nombre", "Apellido", "0000000", "mail@gmail.com", "Ciudad", "Pais", null);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testConstructorSinParametros() {
        Usuario unUsuario = new Usuario();
        assertEquals("Sin-Nombre", unUsuario.getNombre());
        assertEquals("Sin-Apellido", unUsuario.getApellido());
        assertEquals("", unUsuario.getTelefono());
        assertEquals("Sin-Mail", unUsuario.getMail());
        assertEquals("Sin-Ciudad", unUsuario.getCiudad());
        assertEquals("Sin-Pais", unUsuario.getPais());
        assertEquals(new ArrayList<>(), unUsuario.getActividades());
        assertEquals(new ArrayList<>(), unUsuario.getAnimalesApadrinados());
    }

    @Test
    public void testCosntructorPorParametros() {
        Usuario unUsuario = new Usuario("nombre", "apellido", "22035458", "email@email.com", "Montevideo", "Uruguay", new ArrayList<>());
        String resNom = unUsuario.getNombre();
        String resApellido = unUsuario.getApellido();
        String resTelefono = unUsuario.getTelefono();
        String resMail = unUsuario.getMail();
        String resCiudad = unUsuario.getCiudad();
        String resPais = unUsuario.getPais();
        ArrayList<Actividad> resActividades = unUsuario.getActividades();
        ArrayList<Animal> resAnimalesApadrinados = unUsuario.getAnimalesApadrinados();
        assertEquals("nombre", resNom);
        assertEquals("apellido", resApellido);
        assertEquals("22035458", resTelefono);
        assertEquals("email@email.com", resMail);
        assertEquals("Montevideo", resCiudad);
        assertEquals("Uruguay", resPais);
        assertEquals(new ArrayList<>(), resActividades);
        assertEquals(new ArrayList<>(), resAnimalesApadrinados);
    }
    
    @Test
    public void testCosntructorConDeposito() {
        Usuario unUsuario = new Usuario("nombre", "apellido", "22035458", "email@email.com", "Montevideo", "Uruguay", new ArrayList<>(), "1523.20", "UYU", "Mensual", "Transferencia Bancaria");
        String resNom = unUsuario.getNombre();
        String resApellido = unUsuario.getApellido();
        String resTelefono = unUsuario.getTelefono();
        String resMail = unUsuario.getMail();
        String resCiudad = unUsuario.getCiudad();
        String resPais = unUsuario.getPais();
        String resDeposito = unUsuario.getDeposito();
        String resMoneda = unUsuario.getMoneda();
        String resPeriodo = unUsuario.getPeriodo();
        String resMedio = unUsuario.getMedio();
        ArrayList<Actividad> resActividades = unUsuario.getActividades();
        ArrayList<Animal> resAnimalesApadrinados = unUsuario.getAnimalesApadrinados();
        assertEquals("nombre", resNom);
        assertEquals("apellido", resApellido);
        assertEquals("22035458", resTelefono);
        assertEquals("email@email.com", resMail);
        assertEquals("Montevideo", resCiudad);
        assertEquals("Uruguay", resPais);
        assertEquals(new ArrayList<>(), resActividades);
        assertEquals(new ArrayList<>(), resAnimalesApadrinados);
        assertEquals("1523.20", resDeposito);
        assertEquals("UYU", resMoneda);
        assertEquals("Mensual", resPeriodo);
        assertEquals("Transferencia Bancaria", resMedio);
    }


    @Test
    public void testSetNombre() {
        usuario.setNombre("Marcel");
        String resNom = usuario.getNombre();
        assertEquals("Marcel", resNom);
    }

    @Test
    public void testSetNombreVacio() {
        usuario.setNombre("");
        String resNom = usuario.getNombre();
        assertEquals("Sin-Nombre", resNom);
    }
    
    @Test
    public void testSetApellido() {
        usuario.setApellido("Apellido");
        String apellido = usuario.getApellido();
        assertEquals("Apellido", apellido);
    }

    @Test
    public void testSetApellidoVacio() {
        usuario.setApellido("");
        String apellido = usuario.getApellido();
        assertEquals("Sin-Apellido", apellido);
    }
    
    @Test
    public void testSetTelefono() {
        usuario.setTelefono("22035458");
        String telefono = usuario.getTelefono();
        assertEquals("22035458", telefono);
    }

    @Test
    public void testSetTelefonoVacio() {
        usuario.setTelefono("");
        String telefono = usuario.getTelefono();
        assertEquals("", telefono);
    }

    @Test
    public void testSetMail() {
        usuario.setMail("marcel@gmail.com");
        String resMail = usuario.getMail();
        assertEquals("marcel@gmail.com", resMail);
    }

    @Test
    public void testSetMailVacio() {
        usuario.setMail("");
        String resMail = usuario.getMail();
        assertEquals("Sin-Mail", resMail);
    }
    
    @Test
    public void testSetCiudad() {
        usuario.setCiudad("Montevideo");
        String ciudad = usuario.getCiudad();
        assertEquals("Montevideo", ciudad);
    }

    @Test
    public void testSetCiudadVacio() {
        usuario.setCiudad("");
        String ciudad = usuario.getCiudad();
        assertEquals("Sin-Ciudad", ciudad);
    }
    
    @Test
    public void testSetPais() {
        usuario.setPais("Uruguay");
        String pais = usuario.getPais();
        assertEquals("Uruguay", pais);
    }

    @Test
    public void testSetPaisVacio() {
        usuario.setPais("");
        String pais = usuario.getPais();
        assertEquals("Sin-Pais", pais);
    }
    
    @Test
    public void testSetId() {
        usuario.setId(1);
        Integer id = usuario.getId();
        assertTrue(1 == id);
    }

    @Test
    public void testGetActividadesElementoAgregado() {
        Actividad act = new ActividadCualquiera();
        usuario.getActividades().add(act);
        assertEquals(act, usuario.getActividades().get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetActividadesSinElementos() {
        assertEquals(null, usuario.getActividades().get(0));
    }

    @Test
    public void testGetAnimalesApadrinadosElementoAgregado() {
        ArrayList<Animal> animales = new ArrayList();
        Animal animal = new Animal();
        animales.add(animal);
        usuario.setAnimalesApadrinados(animales);
        assertEquals(animal, usuario.getAnimalesApadrinados().get(0));
    }

    @Test
    public void testSetAnimalesApadrinadosVacio() {
        usuario.setAnimalesApadrinados(null);
        ArrayList<Animal> animalesApadrinados = usuario.getAnimalesApadrinados();
        assertEquals(null, animalesApadrinados);
    }
    
    @Test
    public void testSetAnimalesApadrinados() {
        ArrayList<Animal> animales = new ArrayList();
        Animal animal = new Animal();
        animales.add(animal);
        Animal otroAnimal = new Animal();
        animales.add(otroAnimal);
        usuario.setAnimalesApadrinados(animales);
        assertEquals(animales, usuario.getAnimalesApadrinados());
    }
    
    @Test
    public void testSetEsPadrino() {
        usuario.setEsPadrino(true);
        Boolean esPadrino = usuario.getEspadrino();
        assertEquals(true, esPadrino);
    }
    
    
    @Test
    public void testSetDeposito() {
        usuario.setDeposito("1099.99");
        String resDeposito = usuario.getDeposito();
        assertEquals("1099.99", resDeposito);
    }
    
    @Test
    public void testSetMoneda() {
        usuario.setMoneda("UYU");
        String resMoneda = usuario.getMoneda();
        assertEquals("UYU", resMoneda);
    }
    
    @Test
    public void testSetPeriodo() {
        usuario.setPeriodo("Mensual");
        String resPeriodo = usuario.getPeriodo();
        assertEquals("Mensual", resPeriodo);
    }
    
    @Test
    public void testSetMedio() {
        usuario.setMedio("Transferencia Bancaria");
        String resMedio = usuario.getMedio();
        assertEquals("Transferencia Bancaria", resMedio);
    }
}
